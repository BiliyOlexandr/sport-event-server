<?php
namespace App\Controllers\Common;


use App\Controllers\Base\BaseController;
use App\Controllers\Injection;
use App\Globals\GlobalData;
use App\Model\Event;
use App\Model\Filter;
use App\Model\Participant;
use App\Model\Talk;
use App\Model\User;
use App\Model\Utils\ErrorComment;
use App\Model\Utils\Response;
use Exception;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Firebase\Auth\Token\Exception\InvalidToken;
use function GuzzleHttp\Psr7\str;
use Kreait\Firebase\Exception\AuthException;
use Kreait\Firebase\Exception\FirebaseException;
use Kreait\Firebase\Factory;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use VK\Client\VKAPIClient;
use VK\Exceptions\VKAPIException;
use VK\Exceptions\VKClientException;


/**
 * Class ClientController
 * @package App\Controllers
 */
class CommonController extends BaseController implements CommonRequest {


    /**
     * ClientController constructor.
     * @param Injection $injection
     */
    public function __construct(Injection $injection)
    {
        parent::__construct($injection);
        $this->user = new User();
        $this->TAG = 'CommonController';
    }

    /**
     * @param Injection $injection
     * @return CommonController
     */
    public static function Run(Injection $injection) : self
    {
        $obj = new CommonController($injection);
        if (method_exists($obj, $obj->action)) {
            $obj->handle();
        } else {
            $obj->setResponse(Response::badURI('Bad api'));
        }
        return $obj;
	}

    public function handle()
    {
        try {
            $this->middleware(new CommonMeiddleware($this->validation, $this->request, $this->user));
        } catch (Exception $e) {
            print_log(__FILE__, __LINE__, $e->getMessage());
            $this->setResponse(Response::badData('Bad inner data'));
            return;
        }
        print_log(__FILE__, __LINE__, $this->action);
        $this->{$this->action}();
    }

    function getCountry()
    {
        $this->setResponse(Response::create([
            'country' => $this->getDataByLng(GlobalData::getInstance()->getCountries(), $this->request['lang']),
        ]));
        return json_encode($this->response);
    }

    function setCity(){
        $city = $this->getObject([
            'id', 'RU', 'EN', 'country', 'lat', 'lng'
        ]);

        $cities = $this->db->where('country', $city['country'])->where('RU', $city['RU'])->get(CITY);
        if (count($cities) > 0){
            $this->setResponse(Response::duplicate());
        } else {
            try {
                $this->db->insert(CITY, $city);
                $this->setResponse(Response::create());
            } catch (Exception $e) {
                $this->setResponse(Response::error());
            }
        }
    }

    function getCityGeoYandex()
    {
        $lang = 'en_EN';
        if ($this->request['lang'] == 'RU'){
            $lang = 'ru_RU';
        }
        $url = 'https://geocode-maps.yandex.ru/1.x/?apikey='.YANDEX_API_KEY.'&geocode='.$this->request['lng'].','.$this->request['lat'].'&format=json&kind=locality&lang='.$lang;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $res = curl_exec($ch);
        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($info !== 200){
            $this->setResponse(Response::notFound());
            return;
        }
        $res = json_decode($res, true);
        //print_log(__FILE__, __LINE__, $res);
        //print_log(__FILE__, __LINE__, $res['response']);
        //print_log(__FILE__, __LINE__, $res['response']['GeoObjectCollection']['featureMember']);

        if (isset($res['response']['GeoObjectCollection']['featureMember'])){
            $found = [];

            $locality = $res['response']['GeoObjectCollection']['featureMember'];



            if (count($locality) > 0 && isset($locality[0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['CountryNameCode'])){
                $countryCode = $locality[0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['CountryNameCode'];
                $ind = indexOf(
                    GlobalData::getInstance()->getCountries(),
                    'iso',
                    $countryCode
                );

                $cities = [];
                if ($ind > -1) {
                    print_log(__FILE__, __LINE__, $ind);
                    print_log(__FILE__, __LINE__, $locality[0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['Locality']['LocalityName']);
                    print_log(__FILE__, __LINE__, $locality[0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['SubAdministrativeArea']['Locality']['LocalityName']);

                    if (
                        isset($locality[0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea'])
                    ){

                        if (isset($locality[0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['Locality']['LocalityName'])){
                            $city = $locality[0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['Locality']['LocalityName'];
                        } else if (isset($locality[0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['SubAdministrativeArea']['Locality']['LocalityName'])){
                            $city = $locality[0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['SubAdministrativeArea']['Locality']['LocalityName'];
                        } else {
                            $this->setResponse(Response::notFound(0));
                            return;
                        }
                        $cities[] = [
                            'RU' => $city,
                            'lat' => explode(' ', $locality[0]['GeoObject']['Point']['pos'])[1],
                            'lng' => explode(' ', $locality[0]['GeoObject']['Point']['pos'])[0],
                            'country' => $countryCode
                        ];
                    }


                    $this->setResponse(Response::create([
                        'cities' => $cities,
                    ]));
                } else {
                    $this->setResponse(Response::notFound(1));
                }
            } else {
                $this->setResponse(Response::notFound(2));
            }

        } else {
            $this->setResponse(Response::notFound(3));
        }
    }


    function getCityGeo()
    {
        if ($this->request['lang'] == 'RU'){
            $lang='ru_RU';
        } else {
            $lang='en_US';
        }
        //$cityID = $this->db->rawQueryValue("(select * from ".CITY." order by abs(lat - ".$this->request['lat'].") limit 1) union (select * from ".CITY." order by abs(lng - ".$this->request['lng'].") limit 1)");
        //$cityID = $this->db->rawQueryValue("select * from ".CITY." order by abs(lat - ".$this->request['lat'].") limit 1");
        $lat = $this->request['lat'];
        $lng = $this->request['lng'];

        //поиск ближайшего города
        $cityID = $this->db->rawQueryValue("select id, RU, 111.045 * DEGREES(ACOS(COS(RADIANS($lat))
                    * COS(RADIANS(lat))
                    * COS(RADIANS(lng) - RADIANS($lng))
                    + SIN(RADIANS($lat))
                    * SIN(RADIANS(lat)))) 
                  AS distance from ".CITY." ORDER BY distance limit 1");

        //$this->getDataByLng(GlobalData::getInstance()->getCountries(), $this->request['lang']),
        $cityArr = $this->db->where('id', $cityID)->get(CITY);
        if (count($cityArr) > 0){
            $city = $this->getDataByLng($cityArr, $this->request['lang'])[0];
            $country = $this->getDataByLng($this->db->where('iso', $city['country'])->get(COUNTRY), $this->request['lang'])[0];
        } else {
            $city= $country = null;
        }


        $this->setResponse(Response::create([
            'city' => $city,
            'country' => $country
        ]));


        //print_log(__FILE__, __LINE__, $this->db->getLastQuery());
        //print_log(__FILE__, __LINE__, $cityID);
        //$this->setResponse([]);
        /*$url = 'https://geocode-maps.yandex.ru/1.x/?apikey='.YANDEX_API_KEY.'&geocode='.$this->request['lng'].','.$this->request['lat'].'&format=json&kind=locality&lang='.$lang;
        //https://api-maps.yandex.ru/services/coverage/v2/?l=map&ll=27.55000000,53.90000000&z=11&lang=ru_RU&callback=jsonp_yandex_coverage__l_map_ll_27_55000000_53_90000000_z_11_lang_ru_RU
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $res = curl_exec($ch);
        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($info !== 200){
            $this->setResponse(Response::notFound());
            return;
        }
        $res = json_decode($res, true);
        //print_log(__FILE__, __LINE__, $res);
        //print_log(__FILE__, __LINE__, $res['response']);
        //print_log(__FILE__, __LINE__, $res['response']['GeoObjectCollection']);

        if (isset($res['response']['GeoObjectCollection']['featureMember'])){
            $found = [];

            $locality = $res['response']['GeoObjectCollection']['featureMember'];

            if (count($locality) > 0 && isset($locality[0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['CountryNameCode'])){
                $countryCode = $locality[0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['CountryNameCode'];
                $ind = indexOf(
                    GlobalData::getInstance()->getCountries(),
                    'iso',
                    $countryCode
                );

                $cities = [];
                if ($ind > -1) {
                    foreach ($locality as $loc){
                        if (
                            isset($loc['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['Locality']['LocalityName']) &&
                            in_array($loc['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['Locality']['LocalityName'], $cities) === false
                        ){

                            $cities[] = $loc['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['Locality']['LocalityName'];
                        }
                    }
                    //print_log(__FILE__, __LINE__, $cities);

                    if (count($cities) > 0){
                        $cities = $this->db->where($this->request['lang'], $cities, 'IN')->get(CITY, null, ['id', $this->request['lang']]);
                        //print_log(__FILE__, __LINE__, $this->db->getLastQuery());
                    }
                    //print_log(__FILE__, __LINE__, $cities);


                    $country = GlobalData::getInstance()->getCountries()[$ind];
                    if ($this->request['lang'] == 'RU'){
                        unset($country['EN']);
                    } else {
                        unset($country['RU']);
                    }

                    if ($this->request['lang'] == 'RU'){
                        $country['name'] = $country['RU'];
                    } else {
                        $country['name'] = $country['EN'];
                    }
                    unset($country['EN']);
                    unset($country['RU']);


                    $this->setResponse(Response::create([
                        'country' => $country,
                        'cities' => $this->getDataByLng($cities, $this->request['lang']),
                    ]));
                } else {
                    $this->setResponse(Response::notFound());
                }
            } else {
                $this->setResponse(Response::notFound());
            }

        } else {
            $this->setResponse(Response::notFound());
        }*/
    }


    function getCityAutocomplete()
    {
        if (isset($this->request['country_iso'])){
            $this->db->where('country', $this->request['country_iso']);
        }

        $cities = $this->db->where($this->request['lang'], $this->request['word'].'%', 'like')->get(CITY);
        print_log(__FILE__, __LINE__, $this->db->getLastQuery());

        $this->setResponse(Response::create([
            'cities' => $this->getDataByLng(
                $cities,
                $this->request['lang'])
        ]));
    }

    protected function getCityIndexYandex(array $locality){
        foreach ($locality as $local){
            if (isset($local['GeoObject'])){
                $ind = indexOf(
                    GlobalData::getInstance()->getCountries(),
                    $this->request['lang'],
                    $local['GeoObject']['metaDataProperty']['description'],
                    true
                );
                if ($ind > -1){
                    return $ind;
                }
            }
        }
        return -1;
    }


    function getCommonData()
    {
        $this->setResponse(Response::create([
            'sport_kind' => $this->getDataByLng(GlobalData::getInstance()->getSportKind(), $this->request['lang']),
            'type_participant' => $this->getDataByLng(GlobalData::getInstance()->getTypeParticipant(), $this->request['lang'], ['age_min', 'age_max']),
            'type_event' => $this->getDataByLng(GlobalData::getInstance()->getTypeEvent(), $this->request['lang']),
            'country' => $this->getDataByLng(GlobalData::getInstance()->getCountries(), $this->request['lang']),
        ]));
    }




    function loginFirebase()
    {
        //$idTokenString = 'eyJhbGciOiJSUzI1NiIsImtpZCI6IjVlZGQ5NzgyZDgyMDQwM2VlODUxOGM0YWFiYjJiOWZlMzEwY2FjMTIiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJhenAiOiI5NTc0OTk1OTU1NjUtYmIxMXFlazdnNDZuNTU0OGdnbjR1MmhoZ3Y5dDRkNTguYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdWQiOiI5NTc0OTk1OTU1NjUtanJrZ2QxM3JpdXVxaGJwNDlqdDJmdHZ1cG1wMXQwZG8uYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMTczMTEyNTU4OTUyOTMwMTIxMTkiLCJlbWFpbCI6InJhcm9nc3ZAZ21haWwuY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsIm5hbWUiOiLQodC10YDQs9C10Lkg0KDQsNGA0L7QsyIsInBpY3R1cmUiOiJodHRwczovL2xoNC5nb29nbGV1c2VyY29udGVudC5jb20vLXNJNERRcEtScDhvL0FBQUFBQUFBQUFJL0FBQUFBQUFBQUFBL0FDSGkzcmZPU1dvUDhtSnh6a2xvWVFkeExpSEloSF9xcFEvczk2LWMvcGhvdG8uanBnIiwiZ2l2ZW5fbmFtZSI6ItCh0LXRgNCz0LXQuSIsImZhbWlseV9uYW1lIjoi0KDQsNGA0L7QsyIsImxvY2FsZSI6InJ1IiwiaWF0IjoxNTc5MDEyODQxLCJleHAiOjE1NzkwMTY0NDF9.D63sk9EVxU6gG7GrIczqbTs2ZdKsTABubrgO3mQLtinWGkst3z2FnV4H6ZMT-WnvCgWolzc6aJCEc1RcnpXHFPJ1AGOB6hrh9K8r29qn9bgeiDID8rE_oGFS2OHeF4vQ2VcyHtwiDLzKqly-y7u-KS_TgbZ57RBVbcssr8blkJ1jOh7CU3dQrzyYxXA4Vs9L8K3RpFBgUBd3Xb4UAwDckWUaD-JbR_yMsDj803w7P1FpnKxVAyPmuHIPpBouBiz4zr2Z7kxwH4_aNUlKxnty_wKmSBIMu3_paM74cuPMTEcdGW-dbO3U7-Keu_BFIHq43LJup6a3vzDSxOktezF12w';
        //$idTokenString = 'eyJhbGciOiJSUzI1NiIsImtpZCI6IjVlZGQ5NzgyZDgyMDQwM2VlODUxOGM0YWFiYjJiOWZlMzEwY2FjMTIiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJhenAiOiI5NTc0OTk1OTU1NjUtYmIxMXFlazdnNDZuNTU0OGdnbjR1MmhoZ3Y5dDRkNTguYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdWQiOiI5NTc0OTk1OTU1NjUtanJrZ2QxM3JpdXVxaGJwNDlqdDJmdHZ1cG1wMXQwZG8uYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMTczMTEyNTU4OTUyOTMwMTIxMTkiLCJlbWFpbCI6InJhcm9nc3ZAZ21haWwuY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsIm5hbWUiOiLQodC10YDQs9C10Lkg0KDQsNGA0L7QsyIsInBpY3R1cmUiOiJodHRwczovL2xoNC5nb29nbGV1c2VyY29udGVudC5jb20vLXNJNERRcEtScDhvL0FBQUFBQUFBQUFJL0FBQUFBQUFBQUFBL0FDSGkzcmZPU1dvUDhtSnh6a2xvWVFkeExpSEloSF9xcFEvczk2LWMvcGhvdG8uanBnIiwiZ2l2ZW5fbmFtZSI6ItCh0LXRgNCz0LXQuSIsImZhbWlseV9uYW1lIjoi0KDQsNGA0L7QsyIsImxvY2FsZSI6InJ1IiwiaWF0IjoxNTc5MDEyODQxLCJleHAiOjE1NzkwMTY0NDF9.D63sk9EVxU6gG7GrIczqbTs2ZdKsTABubrgO3mQLtinWGkst3z2FnV4H6ZMT-WnvCgWolzc6aJCEc1RcnpXHFPJ1AGOB6hrh9K8r29qn9bgeiDID8rE_oGFS2OHeF4vQ2VcyHtwiDLzKqly-y7u-KS_TgbZ57RBVbcssr8blkJ1jOh7CU3dQrzyYxXA4Vs9L8K3RpFBgUBd3Xb4UAwDckWUaD-JbR_yMsDj803w7P1FpnKxVAyPmuHIPpBouBiz4zr2Z7kxwH4_aNUlKxnty_wKmSBIMu3_paM74cuPMTEcdGW-dbO3U7-Keu_BFIHq43LJup6a3vzDSxOktezF12w';

        $factory = (new Factory())->withServiceAccount(\Kreait\Firebase\ServiceAccount::fromJsonFile(DIR.'App/Config/firebase_credentials.json'));
        $auth = $factory->createAuth();
        //$verifiedIdToken = $auth->verifyIdToken($idTokenString, true);
        //print_log(__FILE__, __LINE__, $verifiedIdToken);

        //dH1zsqPQtDdDoxZi0yKj1fRRSV22
        try {
            $userDataFirebase = (array)$auth->getUser($this->request['firebase_user_id']);
            //print_log(__FILE__, __LINE__, $auth->getUser('A0io3j2R45hORN5Nttj6HqtRLQR2'));
            //print_log(__FILE__, __LINE__, $userDataFirebase);
            /**{"uid":"A0io3j2R45hORN5Nttj6HqtRLQR2","email":"rarogsv@gmail.com","emailVerified":true,"displayName":"\u0421\u0435\u0440\u0433\u0435\u0439 \u0420\u0430\u0440\u043e\u0433","photoUrl":"https:\/\/lh4.googleusercontent.com\/-sI4DQpKRp8o\/AAAAAAAAAAI\/AAAAAAAAAAA\/ACHi3rfOSWoP8mJxzkloYQdxLiHIhH_qpQ\/s96-c\/photo.jpg","phoneNumber":null,"disabled":false,"metadata":{"createdAt":"2020-01-14T14:58:32+00:00","lastLoginAt":"2020-01-14T14:58:32+00:00"},"providerData":[{"uid":"117311255895293012119","displayName":"\u0421\u0435\u0440\u0433\u0435\u0439 \u0420\u0430\u0440\u043e\u0433","email":"rarogsv@gmail.com","photoUrl":"https:\/\/lh4.googleusercontent.com\/-sI4DQpKRp8o\/AAAAAAAAAAI\/AAAAAAAAAAA\/ACHi3rfOSWoP8mJxzkloYQdxLiHIhH_qpQ\/s96-c\/photo.jpg","providerId":"google.com","phoneNumber":null}],"passwordHash":null,"customAttributes":[],"tokensValidAfterTime":"2020-01-14T14:58:32+00:00"}*/
        } catch (AuthException $e) {
            print_log(__FILE__, __LINE__, $e->getMessage());
            $this->setResponse(Response::badData(ErrorComment::BAD_TOKEN[$this->request['lang']]));
            return;
        } catch (FirebaseException $e) {
            print_log(__FILE__, __LINE__, $e->getMessage());
            $this->setResponse(Response::badData(ErrorComment::BAD_TOKEN[$this->request['lang']]));
            return;
        }  catch (Exception $e) {
            print_log(__FILE__, __LINE__, $e->getMessage());
            $this->setResponse(Response::badData(ErrorComment::BAD_TOKEN[$this->request['lang']]));
            return;
        }

        $this->user=User::Search(['email' => $userDataFirebase['email']]);

        //print_log(__FILE__, __LINE__, $this->user->getId());
        if ($this->user->getId() == -1){
            $userData = $this->getLoginUserObject(['lang', 'country_id']);
            $userData['name'   ]   = $userDataFirebase['displayName'];
            $userData['email'  ]   = strtolower($userDataFirebase['email']);
            $userData['created']   = time();
            $userData['token'  ]   = $this->generateToken();
            $userData['icon'   ]   = $userDataFirebase['photoUrl'];


            $this->user = User::Create($userData);
            if ($this->user->save() > -1){
                $this->setResponse(Response::create(['token'=>$this->getUserToken($this->user)]));
            } else {
                $this->setResponse(Response::error());
            }
        } else {
            $this->setResponse(Response::create(['token'=>$this->getUserToken($this->user)]));
        }
    }


    function loginVK()
    {
        $vk = new VKAPIClient();
        $access_token = '1a231e741a231e741a231e748c1a4eebd411a231a231e7447e9c39a60060720cf2bbba0';
        try {
            //Access-Token - 6a258db556ef819116da49915821fd288a2549156020f0399e91436d652decf44e70bc3f7e9f886532c1e
            //$response = (array)$vk->account()->getProfileInfo('6a258db556ef819116da49915821fd288a2549156020f0399e91436d652decf44e70bc3f7e9f886532c1e');
            //{"first_name":"\u0421\u0435\u0440\u0433\u0435\u0439","last_name":"\u0420\u0430\u0440\u043e\u0433","bdate":"1.6.1950",
            //"bdate_visibility":0,"city":{"id":280,"title":"\u0425\u0430\u0440\u044c\u043a\u043e\u0432"},"country":{"id":2,"title":"\u0423\u043a\u0440\u0430\u0438\u043d\u0430"},
            //"home_town":"","phone":"+380 **** * ** 14","relation":0,"relation_requests":[{"id":17527175,"first_name":"\u0418\u0440\u0438\u043d\u0430","last_name":"\u0420\u0430\u0440\u043e\u0433"}],
            //"sex":2,"status":""}

            //User_id - 19892809
            $response = $vk->users()->get($access_token, [
                'user_ids'  => [$this->request['vk_user_id']],
                'fields'    => ['sex', 'photo_200'],
            ])[0];

            //{"id":19892809,"first_name":"Sergey","last_name":"Rarog","sex":2,"photo_200":"https:\/\/sun1-29.userapi.com\/fGIMKfW9hiOXitPAmBQBlX7bHR69TpknpiofCQ\/IE3rz63zubg.jpg?ava=1"}
            print_log(__FILE__, __LINE__, $response);
            //return;

            if($response['sex'] <= 2 && $response['sex'] >= 0){
                $response['sex'] = 0;
            }

            $this->user=User::Search(['email' => $this->request['email']]);
            if ($this->user->getId() == -1){
                $userData = $this->getLoginUserObject(['lang', 'country_id']);

                $userData['name'   ]   = $response['first_name'].' '.$response['last_name'];
                $userData['email'  ]   = strtolower($this->request['email']);
                $userData['lang'  ]   = $this->request['lang'];
                $userData['created']   = time();
                $userData['gender' ]   = $response['sex'];
                $userData['token'  ]   = $this->generateToken();
                $userData['icon'   ]   = $response['photo_200'];
                $this->user = User::Create($userData);

                if ($this->user->save() > -1){
                    $this->setResponse(Response::create(['token'=>$this->getUserToken($this->user)]));
                } else {
                    $this->setResponse(Response::error());
                }
            } else {
                $this->setResponse(Response::create(['token'=>$this->getUserToken($this->user)]));
            }
            print_log(__FILE__, __LINE__, $response);
        } catch (VKAPIException $e) {
            $this->setResponse(Response::badData(ErrorComment::BAD_TOKEN[$this->request['lang']]));
        } catch (VKClientException $e) {
            $this->setResponse(Response::badData(ErrorComment::BAD_TOKEN[$this->request['lang']]));
        } catch (Exception $e) {
            $this->setResponse(Response::badData(ErrorComment::BAD_TOKEN[$this->request['lang']]));
        }
    }

    function loginFB()
    {
        try {
            $fb = new Facebook([
                'app_id' => FB_ID,
                'app_secret' => FB_KEY,
                'default_graph_version' => 'v5.0',
            ]);
            $response = $fb->get('/me?fields=id,name,email', $this->request['fb_token']);
            //$response = $fb->get('/me?fields=id,name,email', 'EAAfnhT8FRQQBAEknaDBNOUUfNmRmyZBlLXPh0EcI6wU9v0j1l0TBz8aSC1cm522aoeZAnaPObKZAWAYYLtjYbhM9oe75bwtlJJGob67uQimEX2iDO36bUGAyEtzfrSwW4jgCdoZBUjrtQ6LCzada77LcyrZC5Apbj5sxiNQJkU3JBzp45wLgpOSG3r4UZB4D4ZBtf8hXSwFOC5uWWq2blpZCiQnb86GFlNBSQNNkXiUz9KY373aIXhYi');
            //$response = $fb->get('/me', 'EAAfnhT8FRQQBAEknaDBNOUUfNmRmyZBlLXPh0EcI6wU9v0j1l0TBz8aSC1cm522aoeZAnaPObKZAWAYYLtjYbhM9oe75bwtlJJGob67uQimEX2iDO36bUGAyEtzfrSwW4jgCdoZBUjrtQ6LCzada77LcyrZC5Apbj5sxiNQJkU3JBzp45wLgpOSG3r4UZB4D4ZBtf8hXSwFOC5uWWq2blpZCiQnb86GFlNBSQNNkXiUz9KY373aIXhYi');
            $userFB = $response->getGraphUser();

            print_log(__FILE__, __LINE__, $userFB->getId());
            print_log(__FILE__, __LINE__, $userFB->getEmail());
            print_log(__FILE__, __LINE__, $userFB->getFirstName());
            print_log(__FILE__, __LINE__, $userFB->getLastName());
            print_log(__FILE__, __LINE__, $userFB->getPicture());
            //$this->setResponse(Response::badData());
            //return;
            $userData = $this->getLoginUserObject(['lang', 'country_id']);

            if ($userFB->getEmail() == null || strlen($userFB->getEmail()) == 0){
                $this->setResponse(Response::notFound(ErrorComment::EMAIL_NOT_FOUND[$this->request['lang']]));
                return;
            }

            $this->user=User::Search(['email' => $userFB->getEmail()]);

            if ($this->user->getId() == -1){
                $userData['email']   = strtolower($userFB->getEmail());
                $userData['sex'] = 0;
                $userData['gender'] = 'Пупкин Пупс';//$userFB->getFirstName().' '.$userFB->getLastName();
                $userData['lang'  ]   = $this->request['lang'];
                $userData['created']   = time();
                $userData['token'  ]   = $this->generateToken();
                $userData['icon'   ]   = "http://graph.facebook.com/".$userFB->getId()."/picture?type=square";
                $this->user = User::Create($userData);

                if ($this->user->save() > -1){
                    $this->setResponse(Response::create(['token'=>$this->getUserToken($this->user)]));
                } else {
                    $this->setResponse(Response::error());
                }
            } else {
                $this->setResponse(Response::create(['token'=>$this->getUserToken($this->user)]));
            }
        } catch (FacebookSDKException $e) {
            $this->setResponse(Response::error());
        }

       // $this->setResponse(Response::badData());
    }


    function registration()
    {
        if (strlen($this->request['pass']) < 5){
            $this->setResponse(Response::badData(ErrorComment::SHORT_PASS[$this->request['lang']]));
            return;
        }

        $this->user=User::Search(['email' => $this->request['email']]);

        print_log(__FILE__, __LINE__, $this->user->getId());
        if ($this->user->getId() == -1){
            $userData = $this->getLoginUserObject(['name', 'email', 'pass', 'lang', 'country_id']);
            $userData['created'] = time();
            $userData['token'] = $this->generateToken();
            $this->user = User::Create($userData);
            if ($this->user->save() > -1){
                $this->setResponse(Response::create(['token'=>$this->getUserToken($this->user)]));
            } else {
                $this->setResponse(Response::error());
            }
        } else {
            $this->userInner();
        }
    }

    function login()
    {
        if (strlen($this->request['pass']) < 5){
            $this->setResponse(Response::badData(ErrorComment::SHORT_PASS[$this->request['lang']]));
            return;
        }

        //['pass', 'email', 'device_token', 'device']
        $this->user = User::Search(['email'=>$this->request['email']]);
        if ($this->user->getId() > -1){
            $this->userInner();
        } else {
            $this->setResponse(Response::notFound(ErrorComment::USER_NOT_REG[$this->request['lang']]));
        }
    }

    function recoveryPass()
    {
        $this->user = User::Search(['email'=>$this->request['email']]);
        if ($this->user->getId() > -1){
            try {

                $this->db->where('user_id', $this->user->getId())->delete(RECOVERY_PASS);
            } catch (Exception $e) {
            }
            $token = $this->generateToken();
            $recPassID = $this->db->insert(RECOVERY_PASS, [
                'user_id' => $this->user->getId(),
                'token' => $token,
                'created' => time()
            ]);

            if ($recPassID > 0){
                $mail = new PHPMailer(true);
                $mail->Host       = '193.203.48.73';                    // Set the SMTP server to send through
                //$mail->Host       = 'smtp.ukr.net';                    // Set the SMTP server to send through
                $mail->SMTPAuth   = false;                               // Enable SMTP authentication
                $mail->Username   = 'recovery@sportevent.global';       // SMTP username
                //$mail->Username   = 'rarogs@ukr.net';       // SMTP username
                $mail->Password   = 'IVVvSlrfyo';                       // SMTP password
                //$mail->Password   = '963451rarogsv';                       // SMTP password
                $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;     // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
                $mail->Port       = 143;                                // TCP port to connect to



                //Recipients
                try {
                    $mail->setFrom('recovery@sportevent.global', 'Recovery pass.');
                    //$mail->setFrom('rarogs@ukr.net', 'Mailer1');
                    $mail->addAddress($this->request['email'], 'Recovery pass.');
                    $mail->isHTML(true);                                  // Set email format to HTML
                    $mail->Subject = 'Recovery pass.';
                    $content = 'url?token='.$recPassID.':'.$token;
                    $mail->Body    = 'Перейдите по ссылке-<b>'.$content.'</b>';
                    $mail->send();
                } catch (\PHPMailer\PHPMailer\Exception $e) {
                    print_log(__FILE__, __LINE__, $e->getMessage());
                }


                //send to email
                $this->setResponse(Response::create([]));
            }
        } else {
            $this->setResponse(Response::notFound());
        }
    }

    function recoveryPassComplete()
    {
        $jwt = explode(':', $this->request['token']);
        if (count($jwt)!= 2){
            $this->setResponse(Response::badData());
            return;
        }

        $id = $jwt[0];
        $token = $jwt[1];

        $recPass = null;
        try {
            $recPass = $this->db->where('id', $id)->getOne(RECOVERY_PASS);
        } catch (Exception $e) {
            $this->setResponse(Response::badData());
            return;
        }

        if ($recPass == null || $recPass['token'] != $token){
            $this->setResponse(Response::badData());
            return;
        }


        $this->user = User::Load($recPass['user_id']);

        if ($this->user->getId() > -1){

            try {
                $this->db->where('id', $id)->delete(RECOVERY_PASS);
            } catch (Exception $e) {
            }

            if ($this->user->update(['pass'=>$this->request['pass']])){
                $this->setResponse(Response::create());
            } else {
                $this->setResponse(Response::error());
            }
        } else {
            $this->setResponse(Response::notFound());
        }
    }

    protected function userInner(){
        if ($this->user->get('pass') != $this->request['pass']){
            $this->setResponse(Response::unauth());
        } else {
            if ($this->user->update($this->getLoginUserObject())){
                $this->setResponse(Response::create(['token'=>$this->getUserToken($this->user)]));
            } else {
                $this->setResponse(Response::error());
            }
        }
    }

    protected function getLoginUserObject(array $keys = []) : array
    {
        if (count($keys) > 0){
            $data = $this->getObject($keys);
        }

        if ($this->request['device'] == 2){
            $data['firebase_token_web'] = $this->request['device_token'];
        } else {
            $data['firebase_token_mobile'] = $this->request['device_token'];
        }
        return $data;
    }

    protected function getUserToken(User $user)
    {
        return $user->getId().':'.$user->get('token');
    }



}
