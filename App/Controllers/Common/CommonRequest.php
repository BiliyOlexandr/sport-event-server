<?php
/**
 * Created by PhpStorm.
 * UserVerification: rarog
 * Date: 02.08.2019
 * Time: 15:49
 */

namespace App\Controllers\Common;


use App\Controllers\Base\BaseURIRequest;

interface CommonRequest extends BaseURIRequest
{

    function getCityGeoYandex();

    function getCityGeo();

    function getCountry();

    function getCityAutocomplete();

    function getCommonData();

    function setCity();

    function loginFirebase();

    function loginVK();

    function loginFB();

    function registration();

    function login();

    function recoveryPass();

    function recoveryPassComplete();

}