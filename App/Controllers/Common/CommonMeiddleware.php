<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 03.08.2019
 * Time: 4:34
 */

namespace App\Controllers\Common;



use App\Controllers\Base\BaseMiddleware;
use App\Model\Filter;
use App\Model\User;
use App\Model\Utils\Validation;

class CommonMeiddleware extends BaseMiddleware implements CommonRequest
{

    protected $user;
    public function __construct(Validation $validation, array &$request, User $user=null)
    {
        parent::__construct($validation, $request);
        $this->user = $user;
    }


    public function handle()
    {
    }
//'getGeoPosition', 'getCountry', 'login', 'loginFirebase', 'loginVK', 'loginFB', 'registration', 'recoveryPass', 'recoveryPassComplete'




    function getCountry()
    {
        $keys = ['lang'];

        if (!$this->validation->arrayKeysExists($this->request, $keys) ||
            strlen($this->request['lang']) != 2
        ){
            throw new \Exception();
        }

        $this->request['lang'] = strtoupper($this->request['lang']);
    }

    function getCommonData()
    {
        $keys = ['lang'];

        if (!$this->validation->arrayKeysExists($this->request, $keys) ||
            strlen($this->request['lang']) != 2
        ){
            throw new \Exception();
        }
    }

    function loginFirebase()
    {
        $keys = ['firebase_user_id', 'lang', 'country_id', 'device_token', 'device'];

        if (!$this->validation->arrayKeysExists($this->request, $keys)
        ){
            throw new \Exception();
        }
    }

    function loginVK()
    {
        $keys = ['vk_user_id', 'lang', 'country_id', 'device_token', 'email', 'device'];

        if (!$this->validation->arrayKeysExists($this->request, $keys)
        ){
            throw new \Exception();
        }
    }

    function loginFB()
    {
        $keys = ['fb_token', 'lang', 'country_id', 'device_token', 'device'];

        if (!$this->validation->arrayKeysExists($this->request, $keys) ||
            strlen($this->request['fb_token']) < 10 ||
            strlen($this->request['lang']) != 2 ||
            !is_numeric($this->request['country_id']) ||
            strlen($this->request['device_token']) === 0 ||
            !is_numeric($this->request['device'])
        ){
            throw new \Exception();
        }
    }

    function registration()
    {
        $keys = ['name', 'email', 'pass', 'lang', 'country_id', 'device_token', 'device'];

        if (!$this->validation->arrayKeysExists($this->request, $keys) ||
            !$this->validation->email($this->request['email']) ||
            strlen($this->request['lang']) != 2 ||
            !is_numeric($this->request['country_id']) ||
            strlen($this->request['device_token']) === 0 ||
            !is_numeric($this->request['device'])
        ){
            throw new \Exception();
        }

        $this->request['lang'] = strtoupper($this->request['lang']);
        $this->request['country_id'] = strtoupper($this->request['country']);
    }


    function login()
    {
        $keys = ['pass', 'email', 'device_token', 'device'];

        if (!$this->validation->arrayKeysExists($this->request, $keys) ||
            !$this->validation->email($this->request['email']) ||
            strlen($this->request['device_token']) === 0 ||
            !is_numeric($this->request['device']) ||
            strlen($this->request['device_token']) < 4
        ){
            throw new \Exception();
        }
    }

    function recoveryPass()
    {
        $keys = ['email'];

        if (!$this->validation->arrayKeysExists($this->request, $keys) ||
            !$this->validation->email($this->request['email'])
        ){
            throw new \Exception();
        }
    }

    function recoveryPassComplete()
    {
        $keys = ['token', 'pass'];

        if (!$this->validation->arrayKeysExists($this->request, $keys) ||
            strlen($this->request['pass']) < 4
        ){
            throw new \Exception();
        }
    }


    function getCityAutocomplete()
    {
        $keys = ['lang', 'word'];

        if (!$this->validation->arrayKeysExists($this->request, $keys) ||
            strlen($this->request['lang']) != 2
        ){
            throw new \Exception();
        }

        $this->request['lang'] = strtoupper($this->request['lang']);
    }

    /**
     * @throws \Exception
     */
    function getCityGeo()
    {
        $keys = ['lang', 'lat', 'lng'];

        if (!$this->validation->arrayKeysExists($this->request, $keys) ||
            strlen($this->request['lang']) != 2 ||
            !is_numeric($this->request['lat']) ||
            !is_numeric($this->request['lng'])
        ){
            throw new \Exception();
        }

        $this->request['lang'] = strtoupper($this->request['lang']);
    }

    function getCityGeoYandex()
    {
        // TODO: Implement getCityGeoYandex() method.
    }

    function setCity()
    {
        // TODO: Implement setCity() method.
    }
}