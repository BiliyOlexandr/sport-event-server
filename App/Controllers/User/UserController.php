<?php
namespace App\Controllers\User;

use App\Controllers\Base\BaseController;
use App\Controllers\Injection;
use App\Globals\GlobalData;
use App\Model\Event;
use App\Model\Filter;
use App\Model\Participant;
use App\Model\Talk;
use App\Model\User;
use App\Model\Utils\ErrorComment;
use App\Model\Utils\Response;
use Exception;
use Firebase\Auth\Token\Exception\InvalidToken;
use Kreait\Firebase\Exception\AuthException;
use Kreait\Firebase\Exception\FirebaseException;
use Kreait\Firebase\Factory;


/**
 * Class ClientController
 * @package App\Controllers
 */
class UserController extends BaseController implements URIUserRequest {


    public static $KEYS_EVENT = [];
    public static $KEYS_USERS = ['id', 'name', 'icon', 'email', 'phone', 'event_active'];


    /**
     * ClientController constructor.
     * @param Injection $injection
     */
    public function __construct(Injection $injection)
    {
        parent::__construct($injection);
        $this->user = new User();
        $this->user->db = GlobalData::getInstance()->getDb();
        $this->user->loadLike(['token'=>$injection->data['Authorization']]);
        $this->TAG = 'ClientController';
    }

    /**
     * @param Injection $injection
     * @return UserController
     */
    public static function Run(Injection $injection) : self
    {
        $obj = new UserController($injection);
        if (method_exists($obj, $obj->action)) {
            if ($obj->hasAccept(USER)) {
                $obj->handle();
            }  else {
                $obj->setResponse(Response::unauth());
            }
        } else {
            $obj->setResponse(Response::badURI('Bad api'));
        }
        return $obj;
    }

    public function handle()
    {
        try {
            $this->middleware(new UserMeiddleware($this->validation, $this->request, $this->user));
        } catch (Exception $e) {
            print_log(__FILE__, __LINE__, $e->getMessage());
            $this->setResponse(Response::badData('Bad inner data'));
            return;
        }

        $this->{$this->action}();
    }



    function getCityByGeo()
    {
        // TODO: Implement getCityByGeo() method.
    }

    function getCityAutocomplete()
    {
        // TODO: Implement getCityAutocomplete() method.
    }

    protected function getCityIndexYandex(array $locality){
        foreach ($locality as $local){
            if (isset($local['GeoObject'])){
                $ind = indexOf(
                    GlobalData::getInstance()->getCountries(),
                    $this->request['lang'],
                    $local['GeoObject']['metaDataProperty']['description'],
                    true
                );
                if ($ind > -1){
                    return $ind;
                }
            }
        }
        return -1;
    }




    protected function getDataByLng(array $data, string $lng, array $addDelRow = [])
    {
        foreach ($data as &$d){
            if ($lng == 'RU'){
                $d['name'] = $d['RU'];
            } else {
                $d['name'] = $d['EN'];
            }
            unset($d['EN']);
            unset($d['RU']);
            /**foreach ($addDelRow as $row){
            unset($d[$row]);
            }*/
        }
        return $data;
    }



    function getBaseCity(){
        if (isset($this->request['city_id'])){
            //$response['city'] = array_search()
        } else if (isset($this->request['country_id'])){

            $country = $this->request['country_id'];
            if (!is_array($country)){
                $country = json_decode($country);
            }
            $response = [];
            if (count($country) === 1){
                //$response
            }
        }
    }



    protected function userInner(){
        if ($this->user->get('pass') != $this->request['pass']){
            $this->setResponse(Response::unauth());
        } else {
            if ($this->user->update($this->getLoginUserObject())){
                $this->setResponse(Response::create(['token'=>$this->getUserToken($this->user)]));
            } else {
                $this->setResponse(Response::error());
            }
        }
    }

    protected function getLoginUserObject(array $keys = []) : array
    {
        if (count($keys) > 0){
            $data = $this->getObject($keys);
        }

        if ($this->request['device'] == 2){
            $data['firebase_token_web'] = $this->request['device_token'];
        } else {
            $data['firebase_token_mobile'] = $this->request['device_token'];
        }
        return $data;
    }

    protected function getUserToken(User $user)
    {
        return $user->getId().':'.$user->get('token');
    }




    function getUser(){
        $userData = $this->user->getData(['id','name', 'email', 'lang', 'country_id','is_command', 'city_id', 'phone', 'icon', 'filter', 'event_active']);


        $newMessArr = $this->db->orWhere('user_id_1', $this->user->getId())->orWhere('user_id_2', $this->user->getId())->get(TALK);

        //подал заявку на участие
        $participantUserArr = $this->getUserParticipant();

        $countNewMass = 0;
        foreach ($newMessArr as $newMass)
        {
            if ($newMass['user_id_1'] == $this->user->getId()){
                $countNewMass += $newMass['read_user_1'];
            } else if ($newMass['user_id_2'] == $this->user->getId()){
                $countNewMass += $newMass['read_user_2'];
            }
        }

        //на кого подписан
        $followers = $this->db->where('subscriber_id', $this->user->getId())->get(FOLLOWER, null, ['follower_id']);
        $followers = array_column($followers, 'follower_id');
        foreach ($participantUserArr as $pua){
            if (!in_array($pua['event_id'], $followers)){
                $followers[] = $pua['event_id'];
            }
        }
        $followerUsers = $this->getUsers($followers);


        $userEvents = $this->db->where('user_id', $this->user->getId())->where('published', 1)->where('status', 1)->get(EVENT, null, ['id']);


        //участники ивента юзера
        $participant = [];
        print_log(__FILE__, __LINE__, 3);
        if (count($userEvents)> 0){
            $participant = $this->db//->where('status', 0)
            ->where('event_id', array_column($userEvents, 'id'), 'IN')
                ->get(EVENT_PARTICIPANT);
            $participantUsers = $this->getUsers(array_column($participant, 'user_id'));
            foreach ($participant as &$p){
                $ind = indexOf($participantUsers, 'id', $p['user_id']);
                $p['user'] = $participantUsers[$ind];
                unset($p['user_id']);
            }
        }



        $filter = Filter::LoadArr($userData['filter'])->getData();
        unset($userData['filter']);
        $response = [
            'user' => $userData,
            'filter' => $filter,
            'new_message' =>$countNewMass,

            //ивенты по фильльтру + ивенты на кого подписан
            'events' => $this->getEventByFilter($followers),

            'follower_users'=>$followerUsers, //подписан
            'participants'=>$participant, //участники
            'user_participant' => $participantUserArr
        ];

        $this->setResponse(Response::create($response));
    }

    protected function getUserParticipant(){
        return $this->db->where('user_id', $this->user->getId())->get(EVENT_PARTICIPANT, null, ['id', 'event_id', 'status', 'created']);
    }



    function getEvents() {
        $followers = $this->db->where('subscriber_id', $this->user->getId())->get(FOLLOWER, null, ['follower_id']);

        $followers = array_column($followers, 'follower_id');
//        print_log(__FILE__, __LINE__, $followers);

        $this->setResponse(Response::create([
            'events' => $this->getEventByFilter($followers),
        ]));
        return json_encode($this->response);
    }

    function getEventRecall()
    {
        $this->setResponse(Response::create([
            'recall' => $this->db->where('event_id', $this->request['id'])->get(EVENT_RECALL, null, ['user_id', 'comment', 'created', 'likes'])
        ]));
    }

    function getEventUser()
    {

        /*$this->setResponse(Response::create([
            'event' => $this->db->where('user_id', $this->user->getId())->get(EVENT, null, UserController::$KEYS_EVENT),
        ]));*/

        $this->setResponse(Response::create([
            'event' => $this->eventAddFields(
                $this->db->where('user_id', $this->user->getId())->get(EVENT, null, UserController::$KEYS_EVENT),
                [$this->user->getId()]
            ),
        ]));
    }

    protected function getUsers(array $usersID = []) : array
    {
        if (count($usersID) === 0){return [];}
        return $this->db->where('id', $usersID, 'IN')->get(\USER, null, UserController::$KEYS_USERS);
    }

    protected function getEventByFilter(array $followers = []) : array
    {
        $filter = Filter::LoadArr($this->user->get('filter'));
//        print_log(__FILE__, __LINE__, $filter->getData());
        if (count($filter->get('sport_kind')) > 0){
            $this->db->where('sport_kind_id', $filter->get('sport_kind'), 'IN');
        }

        if (count($filter->get('type_event')) > 0){
            $this->db->where('type_event_id', $filter->get('type_event'), 'IN');
        }

        if (count($filter->get('type_participant')) > 0){
            $this->db->where('type_participant_id', $filter->get('type_participant'), 'IN');
        }

        if (count($filter->get('date')) == 2  && $filter->get('date')[0] > 0 && $filter->get('date')[1]> 0){
            $this->db->where('date_start', [$filter->get('date')[0], $filter->get('date')[0]+86400], 'BETWEEN')
                ->where('date_stop', [$filter->get('date')[1], $filter->get('date')[1]+86400], 'BETWEEN');
        }

        if (count($filter->get('country')) > 0){
            $this->db->where('country_id', $filter->get('country'), 'IN');
        }

        if (count($filter->get('city')) > 0){
            $this->db->where('city_id', $filter->get('city'), 'IN');
        }

        if (count($followers) > 0){
            $this->db->orWhere('user_id', $followers, 'IN');
        }

        $events = $this->db->where('published', 1)->where('status', 1)->where('user_id', $this->user->getId(), 'NOT LIKE')->get(EVENT);
        //print_log(__FILE__, __LINE__, $this->db->getLastQuery().'-'.count($events));

        $userEvents = $this->db->where('status', 1)->where('user_id', $this->user->getId())->get(EVENT);

        $user_id = $this->user->getId();
        foreach ($events as  $event) {
            if((int)$event->user_id == (int)$user_id){
                $event->user = $this->user;
            }
        }

        //print_log(__FILE__, __LINE__, count($filter->get('age')));
        if (count($filter->get('age')) == 2 ){
            $res = [];
            foreach ($events as  $event) {
                /*print_log(__FILE__, __LINE__, (($filter->get('age')[0] >= $event['age_min']
                        && $filter->get('age')[0] <= $event['age_max'])
                    || ($filter->get('age')[1] >= $event['age_min']
                        && $filter->get('age')[1] <= $event['age_max'])));

                print_log(__FILE__, __LINE__, [
                    $filter->get('age')[0] >= $event['age_min'],
                    $filter->get('age')[0] <= $event['age_max'],
                    $filter->get('age')[1] >= $event['age_min'],
                    $filter->get('age')[1] <= $event['age_max']]);*/
                $min =$filter->get('age')[0] < $event['age_min']  || ($filter->get('age')[0] >= $event['age_min'] && $filter->get('age')[0] <= $event['age_max']);
                $max = $filter->get('age')[1] <$event['age_max'] || ($filter->get('age')[1] >= $event['age_min'] && $filter->get('age')[1] <= $event['age_max']);
                if ($min || $max){
                    $res[] = $event;
                }
            }

            print_log(__FILE__, __LINE__, $res);
            $res = array_merge($res, $userEvents);
            $usersID = array_column($res, 'user_id');
            return $this->eventAddFields($res, $usersID);
        } else {
            $events = array_merge($events, $userEvents);
            $usersID = array_column($events, 'user_id');
            return $this->eventAddFields($events, $usersID);
        }
    }

    protected function eventAddFields(array $e = [], array $u = []) : array
    {
        $idCitiesID = array_unique(
            array_merge(
                array_column($e, 'city_id'),
                array_column($u, 'city_id')
            )
        );
        $cities = [];
        /*print_log(__FILE__, __LINE__, array_merge(
            array_column($e, 'city_id'),
            array_column($u, 'city_id')
        ));*/


        //print_log(__FILE__, __LINE__, count($idCitiesID));
        if (count($idCitiesID) > 0){
            $cities = $this->getDataByLng($this->db->where('id', $idCitiesID, 'IN')->get(CITY), $this->user->get('lang'));
        }

        //print_log(__FILE__, __LINE__, count($cities));

        $typeParticipant = $this->getDataByLng(GlobalData::getInstance()->getTypeParticipant(), $this->user->get('lang'));
        $typeEvent = $this->getDataByLng(GlobalData::getInstance()->getTypeEvent(), $this->user->get('lang'));
        $sportKind = $this->getDataByLng(GlobalData::getInstance()->getSportKind(), $this->user->get('lang'));

        $countries = $this->getDataByLng(GlobalData::getInstance()->getCountries(), $this->user->get('lang'));
        $usersEvent = $this->getUsers($u);

        foreach ($e as &$item){
            $indUser = indexOf($usersEvent, 'id', $item['user_id']);


            $indTypeParticipant = indexOf($typeParticipant, 'id', $item['type_participant_id']);
            $indTypeEvent = indexOf($typeEvent, 'id', $item['type_event_id']);
            $indSportKind = indexOf($sportKind, 'id', $item['sport_kind_id']);

            $indCountry = indexOf($countries, 'id', $item['country_id']);
            $indCity = indexOf($cities, 'id', $item['city_id']);


            $item['type_participant'] = $typeParticipant[$indTypeParticipant];
            $item['type_event'] = $typeEvent[$indTypeEvent];
            $item['sport_kind'] = $sportKind[$indSportKind];

            $item['country'] = $countries[$indCountry];
            $item['city'] = $cities[$indCity];

            $item['user'] = $usersEvent[$indUser];

            unset($item['type_participant_id']);
            unset($item['type_event_id']);
            unset($item['sport_kind_id']);
            unset($item['country_id']);
            unset($item['user_id']);
            unset($item['city_id']);
        }
        return $e;
    }

    function setUser()
    {
        if($this->user->update($this->getObject(['email', 'name', 'country_id', 'phone', 'country_id', 'is_command', 'lang']))){
            $this->setResponse(Response::create());
        } else {
            $this->setResponse(Response::error());
        }
    }

    function setFilter()
    {
        $f = Filter::LoadObject($this->getObject(['sport_kind', 'type_event', 'type_participant', 'age', 'country', 'city', 'date']));
        //print_log(__FILE__, __LINE__, $f->getData());
        //print_log(__FILE__, __LINE__, $f->toString());

        if ($this->user->update(['filter' => $f->toString()])){
            $this->setResponse(Response::create());
        } else {
            $this->setResponse(Response::error());
        }
        return json_encode($this->response);
    }

    function getFollowerProfile()
    {
        $events = $this->db->where('user_id', $this->request['user_id'])->where('published', 1)->get(EVENT);
        $recall = [];

        if (count($events) > 0){
            $recall = $this->db->where('event_id', array_column($events, 'id'))->get(EVENT_RECALL, null, ['event_id', 'comment', 'likes']);
        }

        $this->setResponse(Response::create([
            'follower' => count($this->db->where('subscriber_id', $this->user->getId())->get(FOLLOWER, null, ['follower_id'])),
            'subscribers' => count($this->db->where('follower_id', $this->user->getId())->get(FOLLOWER, null, ['follower_id'])),
            'recall' => $recall
        ]));
    }

    function createEvent()
    {
        //продумать как сохранять фото
        $event = Event::Create($this->getObject(['sport_kind_id', 'name', 'type_event_id', 'gender', 'type_participant_id',
            'country_id', 'city_id', 'date_start', 'date_stop', 'description', 'participant_sum', 'participant_ante',
            'description_ante', 'prize_pool', 'age_min', 'age_max', 'icon']));

        $event->set('icon', $this->checkPhoto($this->getObject(['icon'])));


        //'sport_kind_id', 'name', 'type_event_id', 'gender', 'type_participant_id', 'age_min', 'age_max',
        //            'country_id', 'city_id', 'date_start', 'date_stop', 'description', 'participant_sum', 'participant_ante',
        //            'description_ante', 'prize_pool'
        $event->set('user_id', $this->user->getId());
        $event->set('created', time());
        $event->set('published', 0);

        if ($event->save() > -1){
            $this->setResponse(Response::create(['event'=>$event->getData()]));
        } else {
            $this->setResponse(Response::error());
        }
        $out = [
            'status'=>200,
            'event'=>$this->response['event']
        ];
        return json_encode($out);
    }

    function publicEvent()
    {
        //раскоментировать верификация телефона
        /*if (strlen($this->user->get('phone')) === 0){
            $this->setResponse(Response::notFoundPhone());
            return;
        }*/
        $event = Event::Load($this->request['id']);
        $event->update(['published'=>1]);
        $this->user->update(['event_active' => ($this->user->get('event_active')+1)]);
        $subscribers = $this->db->where('follower_id', $this->user->getId())->get(FOLLOWER, null, ['subscriber_id']);
        $this->sendSubscribers($event, Event::STATUS_CREATE, array_column($subscribers, 'subscriber_id'));
        $this->setResponse(Response::create(['event'=>$event->getData()]));
    }


    function updateEvent()
    {
        $event = Event::Load($this->request['id']);

        $old = $event->getData();

        if ($event->getId() > -1){
            //participant_sum
            if($event->get('status') > 1){
                $this->setResponse(Response::badData(ErrorComment::EVENT_CLOSED[$this->user->get('lang')]));
                return;
            }

            $eventData = $this->getObject(['id', 'sport_kind_id', 'name', 'type_event_id', 'gender', 'type_participant_id', 'age_min', 'age_max',
                'country_id', 'city_id', 'date_start', 'date_stop', 'description', 'participant_sum', 'participant_ante',
                'description_ante', 'prize_pool', 'icon']);


            if(!$eventData['icon']){
                $eventData['icon'] = $old['icon'];
            } else {
                $extension = explode('/', explode(':', substr($eventData['icon'], 0, strpos($eventData['icon'], ';')))[1])[1];
                if($extension){
                    $eventData['icon'] = $this->checkPhoto($eventData);
                    unlink('./src/img/events/'.$old['icon']);
                }
            }



            if ($event->update($eventData)){
                $participant = $this->db->where('event_id', $event->getId())->get(EVENT_PARTICIPANT, null, ['user_id']);
                $this->sendSubscribers($event, Event::STATUS_UPDATE, array_column($participant, 'user_id'));
                $this->setResponse(Response::create(['event'=>$event->getData()]));
            } else {
                $this->setResponse(Response::error());
            }

        } else {
            $this->setResponse(Response::notFound());
        }
    }


    protected function sendSubscribers(Event $event, int $what, array $usersID){
        if ($event->get('published') !== 1 || count($usersID) === 0){return;}

        $users = $this->db->where('id', $usersID, 'IN')->get(\USER, null, ['firebase_token_mobile', 'firebase_token_web', 'lang']);

        //отправить уведомление подписчикам
        $tokens = $this->getUsersFirebaseTokenLang($users);

        switch ($what){
            case Event::STATUS_CREATE:
                if (count($tokens['RU']) > 0){
                    User::sendMessages($tokens, [
                        'title' => 'Новый ивент!',
                        'body' => $event->get('name')
                    ]);
                }
                if (count($tokens['EN']) > 0){
                    User::sendMessages($tokens, [
                        'title' => 'New event!',
                        'body' => $event->get('name')
                    ]);
                }
                break;
            case Event::STATUS_DELETE:
                if (count($tokens['RU']) > 0){
                    User::sendMessages($tokens, [
                        'title' => 'Ивент удален!',
                        'body' => $event->get('name')
                    ]);
                }
                if (count($tokens['EN']) > 0){
                    User::sendMessages($tokens, [
                        'title' => 'Event canceled!',
                        'body' => $event->get('name')
                    ]);
                }
                break;
        }
    }


    protected function getUsersFirebaseTokenLang(array $users){
        $tokens = [
            'RU' => [],
            'EN' => []
        ];
        foreach ($users as $user){
            if ($user['lang'] == 'RU'){
                if (strlen($user['firebase_token_mobile']) > 0){
                    $tokens['RU'][] = $user['firebase_token_mobile'];
                }
                if (strlen($user['firebase_token_web']) > 0){
                    $tokens['RU'][] = $user['firebase_token_web'];
                }
            } else {
                if (strlen($user['firebase_token_mobile']) > 0){
                    $tokens['EN'][] = $user['firebase_token_mobile'];
                }
                if (strlen($user['firebase_token_web']) > 0){
                    $tokens['EN'][] = $user['firebase_token_web'];
                }
            }

        }
        return $tokens;
    }



    function delEvent()
    {
        $event = Event::Load($this->request['id']);
        if ($event->getId() > -1){
            //кроме этого надо всех оповестить и изменить что все свободны
            $participant = $this->db->where('event_id', $event->getId())->where('status', [0,1], 'IN')->get(EVENT_PARTICIPANT);

            $this->sendSubscribers($event, Event::STATUS_DELETE, array_column($participant, 'user_id'));

            $event->update(['published' => 0, 'status'=>2]);
            $this->user->update(['event_active' => ($this->user->get('event_active')-1)]);

            //всех кто хотел принять участие - отмена
            try {
                $this->db->where('event_id', $event->getId())->update(EVENT_PARTICIPANT, ['status' => 3]);
            } catch (Exception $e) {
            }
            $this->setResponse(Response::create());
        } else {
            $this->setResponse(Response::notFound());
        }
    }

    /**
     * Оставить отзыв
     * @throws Exception
     */
    function sendEventRecall()
    {
        $event = Event::Load($this->request['event_id']);
        if ($event->getId() == -1){
            $this->setResponse(Response::notFound());
            return;
        }
        if ($event->get('status') != 0){
            $this->setResponse(Response::badData());
            return;
        }

        try {
            $recall = $this->db
                ->where('event_id', $this->request['event_id'])
                ->where('user_id', $this->user->getId())->getOne(EVENT_RECALL);
        } catch (Exception $e) {
            $recall = null;
        }
        if ($recall != null){
            $this->setResponse(Response::duplicate());
            return;
        }

        $data = $this->getObject(['comment', 'event_id', 'likes']);
        $data['created'] = time();
        $data['user_id'] = $this->user->getId();

        $this->db->insert(EVENT_RECALL, $data);
        $this->setResponse(Response::create());
    }

    /**
     * Получить сообщение
     */
    function getTalks()
    {
        $this->setResponse(['talks' => Talk::GetTalkToUser($this->user->getId(), $this->user->getId(), $this->db)]);
    }

    function readTalk()
    {
        Talk::Load($this->request['id'])->read($this->user->getId());
        $this->setResponse(Response::create());
    }

    function sendMessage()
    {
        if ($this->request['id'] > -1){
            $talk = Talk::Load($this->request['id']);
        } else {
            try {
                $talkSearch = $this->db
                    ->where('user_id_1', [$this->user->getId(), $this->request['user_id']], 'IN')
                    ->where('user_id_1', [$this->user->getId(), $this->request['user_id']], 'IN')
                    ->where('event_id', $this->request['event_id'])
                    ->getOne(\TALK);
            } catch (Exception $e) {
                $talkSearch = null;
            }
            if ($talkSearch == null){
                $talk = Talk::Create([
                    'user_id_1' => $this->request['user_id'] < $this->user->getId() ? $this->request['user_id'] : $this->user->getId(),
                    'user_id_2' => $this->request['user_id'] > $this->user->getId() ? $this->request['user_id'] : $this->user->getId(),
                    'event_id' => $this->request['event_id']
                ]);
                $talk->save();
            } else {
                $talk = Talk::Load($talkSearch['id']);
            }
        }

        if ($talk->getId() > -1){
            if ($talk->setMessage($this->user->getId(), $this->request['message'])){

                $recipient = User::Load($this->request['user_id']);
                if ($recipient->get('lang') == 'RU'){
                    $recipient->sendMessage([
                        'title' => 'Новой сообщение!',
                        'body' => $this->request['message']
                    ],
                        ['key'=>User::NEW_MESSAGE]);
                } else {
                    $recipient->sendMessage([
                        'title' => 'New message!',
                        'body' => $this->request['message']
                    ],
                        ['key'=>User::NEW_MESSAGE]);
                }

                $this->setResponse(Response::create(['talk' => $talk->getTalk($this->user->getId())]));
            } else {
                $this->setResponse(Response::error());
            }
        } else {
            $this->setResponse(Response::error());
        }
    }


    /**
     * Подписаться
     */
    function subscribe()
    {
        if (count($this->db->where('subscriber_id', $this->user->getId())->where('follower_id', $this->request['user_id'])->get(FOLLOWER)) === 0){
            try {
                $this->db->insert(FOLLOWER, [
                    'subscriber_id' => $this->user->getId(),
                    'follower_id' => $this->request['user_id'],
                    'created' => time()
                ]);

                //на кого подписан
                $followers = $this->db->where('subscriber_id', $this->user->getId())->get(FOLLOWER, null, ['follower_id']);
                $followers = array_column($followers, 'follower_id');
                $followerUsers = $this->getUsers($followers);

                $this->setResponse(Response::create([
                    'follower_users' => $followerUsers
                ]));
            } catch (Exception $e) {
                $this->setResponse(Response::error());
            }
            $followerUsers[]= $this->request['user_id'];
        } else {
            $this->setResponse(Response::duplicate());
        }
    }

    function unsubscribe()
    {
        $this->db->where('subscriber_id', $this->user->getId())->where('follower_id', $this->request['user_id'])->delete(FOLLOWER);
        //на кого подписан
        $followers = $this->db->where('subscriber_id', $this->user->getId())->get(FOLLOWER, null, ['follower_id']);
        $followers = array_column($followers, 'follower_id');
        $followerUsers = $this->getUsers($followers);

        $this->setResponse(Response::create([
            'follower_users' => $followerUsers
        ]));
    }

    /**
     * Принять участие
     */
    function participant()
    {
        if (strlen($this->user->get('phone')) === 0){
            $this->setResponse(Response::notFoundPhone());
            return;
        }

        $partObj = Participant::Search(['user_id'=>$this->user->getId(), 'event_id'=>$this->request['event_id']]);

        if ($partObj->getId() > -1){
            $this->setResponse(Response::duplicate());
            return;
        }

        $user = User::Load($this->request['user_id']);
        if ($user->getId() == -1){
            $this->setResponse(Response::badData());
            return;
        }

        $event = Event::Load($this->request['event_id']);
        if ($event->getId() == -1 || $event->get('status') !== 1){
            $this->setResponse(Response::badData());
            return;
        }

        $data = [
            'user_id' => $this->user->getId(),
            'event_id' => $this->request['event_id'],
            'created' => time(),
        ];

        $partObj = Participant::Create($data);

        if ($partObj->save()){

            $recipient = User::Load($event->get('user_id'));
            if ($recipient->get('lang') == 'RU'){
                $recipient->sendMessage([
                    'title' => 'Новый участник!',
                    'body' => $this->user->get('name') . ' хочет принять участие в ' . $event->get('name') . '!'
                ],
                    ['key'=>User::NEW_PARTICIPANT]);
            } else {
                $recipient->sendMessage([
                    'title' => 'New participant!',
                    'body' => $this->user->get('name') . ' wants to take part in ' . $event->get('name') . '!'
                ],
                    ['key'=>User::NEW_PARTICIPANT]);
            }

            $this->setResponse(Response::create([
                'participant' => $partObj->getDataToUser()
            ]));
        } else {
            $this->setResponse(Response::error());
        }
    }

    /**
     * Отказаться от участия
     */
    function unParticipant()
    {
        $participant = Participant::Load($this->request['id']);

        if ($participant->getId() == -1){
            $this->setResponse(Response::badData());
            return;
        }

        $statusOld=$participant->get('status');
        $participant->update(['status'=>3]);

        if ($statusOld == 1){
            $event = Event::Load($participant->get('event_id'));
            $event->update(['participant_count' => ($event->get('participant_count')-1)]);


        }

        $this->setResponse(Response::create([
            'participant' => $participant->getDataToUser()
        ]));
    }

    /**
     * Подтвердить участие юзера
     */
    function acceptParticipant()
    {
        $this->participantUpdate($this->request['participant_id'], 1);
    }

    /**
     * Отказать юзеру в участии
     */
    function refuseParticipantEvent()
    {
        $this->participantUpdate($this->request['participant_id'], 2);
    }


    protected function participantUpdate(int $idPart, int $status)
    {
        $participant = Participant::Load($idPart);
        print_log(__FILE__, __LINE__, $participant->get('status'));
        if (($participant->get('status') === $status) || $participant->get('status') > 1){
            $this->setResponse(Response::oldData());
            return;
        }

        $partOldStatus = $participant->get('status');

        if ($participant->update(['status' => $status])){
            $part = $participant->getDataToUser();
            $recipient = User::Load($participant->get('user_id'));
            $part['user'] = $recipient->getData(UserController::$KEYS_USERS);

            //print_log(__FILE__, __LINE__, $participant->get('status'));

            $event = Event::Load($participant->get('event_id'));

            //print_log(__FILE__, __LINE__, [$partOldStatus, $participant->get('status')]);
            if ($partOldStatus === 1 && $status !== 1){
                $event->update(['participant_count' => ($event->get('participant_count')-1)]);
            } else if ($partOldStatus === 0 && $status === 1){
                $event->update(['participant_count' => ($event->get('participant_count')+1)]);
            }


            //отправить уведомление клиенту
            if ($status == 1){
                if ($recipient->get('lang') == 'RU'){
                    $recipient->sendMessage([
                        'title' => 'Подтверждено!',
                        'body' => 'Ваше участие в ' . $event->get('name') . ' подтверждено!'
                    ],
                        ['key'=>User::DENIED_PARTICIPANT]);
                } else {
                    $recipient->sendMessage([
                        'title' => 'Сonfirmed !',
                        'body' => 'Your participation in ' . $event->get('name') . 'is confirmed!'
                    ],
                        ['key'=>User::DENIED_PARTICIPANT]);
                }
            } else if ($status == 2){
                if ($recipient->get('lang') == 'RU'){
                    $recipient->sendMessage([
                        'title' => 'Отказано!',
                        'body' => 'Отказано принять участие в ' . $event->get('name') . '!'
                    ],
                        ['key'=>User::DENIED_PARTICIPANT]);
                } else {
                    $recipient->sendMessage([
                        'title' => 'Denied!',
                        'body' => 'Denied to take part in ' . $event->get('name') . '!'
                    ],
                        ['key'=>User::DENIED_PARTICIPANT]);
                }
            }





            $this->setResponse(Response::create([
                'participant' => $part,
                'event' => $event->getData()
            ]));
        } else {
            $this->setResponse(Response::error());
        }
    }

    function confirmPhone()
    {
        //$idTokenString = 'FXaTtar1ptPvBLsdxCmDl4gCLHy1'; // inner user token

        $factory = (new Factory())->withServiceAccount(\Kreait\Firebase\ServiceAccount::fromJsonFile(DIR.'App/Config/firebase_credentials.json'));
        $auth = $factory->createAuth();

        try {
            $userDataFirebase = (array)$auth->getUser($this->request['firebase_user_id']);
            if ($userDataFirebase['phoneNumber'] != null){
                $this->user->update(['phone'=>$userDataFirebase['phoneNumber']]);
                $this->setResponse(Response::create());
            } else {
                $this->setResponse(Response::badData(ErrorComment::PHONE_NOT_ACCESS[$this->user->get('lang')]));
            }
            //$verifiedIdToken = $auth->verifyIdToken($idTokenString);
            //print_log(__FILE__, __LINE__, $userDataFirebase);

            /**
             * {"localId":"FXaTtar1ptPvBLsdxCmDl4gCLHy1",
             * "providerUserInfo":[{"providerId":"phone","rawId":"+380954848304","phoneNumber":"+380954848304"}],
             * "lastLoginAt":"1565562783923","createdAt":"1565562783923","phoneNumber":"+380954848304","lastRefreshAt":"2019-08-11T22:33:03.923Z"}
             */

        } catch (InvalidToken $e) {
            $this->setResponse(Response::badData('Bad token'));
            print_log(__FILE__, __LINE__, $e->getMessage());
        } catch (AuthException $e) {
            $this->setResponse(Response::badData('Bad token'));
            print_log(__FILE__, __LINE__, $e->getMessage());
        } catch (FirebaseException $e) {
            $this->setResponse(Response::badData('Bad token'));
            print_log(__FILE__, __LINE__, $e->getMessage());
        }


    }

    /**
     * Пожаловаться
     * @throws Exception
     */
    function complaint()
    {
        if (count($this->db->where('user_id', $this->user->getId())->where('event_id', $this->request['event_id'])->get(EVENT_PARTICIPANT)) > 0)
        {
            $this->setResponse(Response::duplicate());
            return;
        }

        $data = $this->getObject(['comment', 'event_id', 'type']);
        $data['created'] = time();
        $data['user_id'] = $this->user->getId();

        if ($this->db->insert(EVENT_COMPLAINT, $data) >0){
            $this->setResponse(Response::create());
        } else {
            $this->setResponse(Response::error());
        }

    }


    /**
     * @param array $points
     * @return bool
     */
    protected function getDistance(array $points){
        $url = "https://route.api.here.com/routing/7.2/calculateroute.json?app_id=HQ7X3X1nKUu2af3mSoj6&app_code=PqLymwZubDBmScioE0gM2Q";
        $i=0;
        foreach ($points as $point){
            $url .= "&waypoint".$i."=geo!".$point[0].",".$point[1];
            $i++;
        }
        $url .= "&mode=fastest;car;traffic:disabled";
        //print_log(__FILE__, __LINE__,$url);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_AUTOREFERER, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 5);
        curl_setopt($curl, CURLOPT_ENCODING, 'gzip,deflate');
        $header = ['Content-Type: text/json'];
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $res = curl_exec($curl);

        /*$res = array(
            'curl_info' => curl_getinfo($curl),
            'curl_error' => curl_errno($curl),
            'curl_error_message' => curl_error($curl),
            'curl_result' => $res_info
        );*/

        curl_close($curl); // close the connection
        if ($res !== false) {
            $res = json_decode($res, true);
            if (!isset($res['response']['route'][0]['summary']['distance'])){
                return false;
            }
            return $res['response']['route'][0]['summary']['distance'];//metr
        } else{
            return false;
        }
    }

    function checkPhoto($data){
        $icon = $data['icon'];
        if($icon){
            $image_64 = $icon;
            $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];
            $replace = substr($image_64, 0, strpos($image_64, ',')+1);
            $image = str_replace($replace, '', $image_64);
            $image = str_replace(' ', '+', $image);

            $image_name = md5(microtime() . random_int(100, 999)) . '.' . $extension;
            if (!file_exists('./src/img/events')) {
                mkdir('./src/img/events', 0777, true);
            }
            file_put_contents('./src/img/events/'. $image_name, base64_decode($image));
            return $image_name;
        }

        return '';
    }

}
