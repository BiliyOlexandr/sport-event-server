<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 03.08.2019
 * Time: 4:34
 */

namespace App\Controllers\User;



use App\Controllers\Base\BaseMiddleware;
use App\Model\Filter;
use App\Model\User;
use App\Model\Utils\Validation;

class UserMeiddleware extends BaseMiddleware implements URIUserRequest
{

    protected $user;
    public function __construct(Validation $validation, array &$request, User $user=null)
    {
        parent::__construct($validation, $request);
        $this->user = $user;
    }


    public function handle()
    {
    }


    function setUser()
    {
        $keys = ['email', 'name', 'country_id', 'phone', 'country_id', 'is_command', 'lang'];

        if (!$this->validation->arrayKeysExists($this->request, $keys) ||
            !is_array($this->request['filter']) ||
            strlen($this->request['lang']) != 2 ||
            !is_numeric($this->request['country_id']) ||
            !is_numeric($this->request['city_id']) ||
            strlen($this->request['device_token']) === 0 ||
            !$this->validation->numberLogic($this->request['is_command'])
        ){
             throw new \Exception();
        }

        $this->request = array_sub($this->request, $keys);
        $this->request = Filter::LoadObject($this->request['filter'])->toString();
    }


    function setFilter()
    {
        $keys = ['sport_kind', 'type_event', 'type_participant', 'age', 'country', 'city', 'date'];
        if (!$this->validation->arrayKeysExists($this->request, $keys)
        ){
            throw new \Exception();
        }
    }



    function subscribe()
    {
        $keys = ['user_id'];
        if (!$this->validation->arrayKeysExists($this->request, $keys) ||
            !is_numeric($this->request['user_id'])
        ){
            throw new \Exception();
        }
    }

    function unsubscribe()
    {
        $keys = ['user_id'];
        if (!$this->validation->arrayKeysExists($this->request, $keys) ||
            !is_numeric($this->request['user_id'])
        ){
            throw new \Exception();
        }
    }

    function participant()
    {
        $keys = ['event_id'];
        if (!$this->validation->arrayKeysExists($this->request, $keys) ||
            !is_numeric($this->request['event_id'])
        ){
            throw new \Exception();
        }
    }


    function unParticipant()
    {
        $keys = ['id'];
        if (!$this->validation->arrayKeysExists($this->request, $keys) ||
            !is_numeric($this->request['id'])
        ){
            throw new \Exception();
        }
    }


    function refuseParticipantEvent()
    {
        $keys = ['participant_id'];

        if (!$this->validation->arrayKeysExists($this->request, $keys) ||
            !is_numeric($this->request['participant_id'])
        ){
            throw new \Exception();
        }
    }


    function acceptParticipant()
    {
        $keys = ['participant_id'];
        if (!$this->validation->arrayKeysExists($this->request, $keys) ||
            !is_numeric($this->request['participant_id'])
        ){
            throw new \Exception();
        }
    }



    function createEvent()
    {
        $keys = ['sport_kind_id', 'name', 'type_event_id', 'gender', 'type_participant_id', 'age_min', 'age_max',
            'country_id', 'city_id', 'date_start', 'date_stop', 'description', 'participant_sum', 'participant_ante',
            'description_ante', 'prize_pool'];

        if (!$this->validation->arrayKeysExists($this->request, $keys)
        ){
            throw new \Exception();
        }
    }



    function updateEvent()
    {
        $keys = ['id', 'sport_kind_id', 'name', 'type_event_id', 'gender', 'type_participant_id', 'age_min', 'age_max',
            'country_id', 'city_id', 'date_start', 'date_stop', 'description', 'participant_sum', 'participant_ante',
            'description_ante', 'prize_pool'];

        if (!$this->validation->arrayKeysExists($this->request, $keys) ||
            !is_numeric($this->request['id'])
        ){
            throw new \Exception();
        }
    }


    function delEvent()
    {
        $keys = ['id'];
        if (!$this->validation->arrayKeysExists($this->request, $keys) ||
            !is_numeric($this->request['id'])
        ){
            throw new \Exception();
        }
    }


    /**
     * События по фильтру и подписанным
     */
    function getEvents()
    {
    }




    function getCityByGeo(){
        // TODO: Implement getCityByGeo() method.
    }


    function getCityAutocomplete()
    {
        // TODO: Implement getCityAutocomplete() method.
    }


    function getUser()
    {
    }


    function confirmPhone()
    {
        $keys = ['firebase_user_id'];
        if (!$this->validation->arrayKeysExists($this->request, $keys)
        ){
            throw new \Exception();
            return;
        }
    }


    function readTalk()
    {
        $keys = ['id'];
        if (!$this->validation->arrayKeysExists($this->request, $keys) ||
            !is_numeric($this->request['id'])
        ){
            throw new \Exception();
            return;
        }
    }

    /**
     * Получить сообщение
     */
    function getTalks()
    {
    }

    function sendMessage()
    {
        $keys = ['id', 'message', 'user_id', 'event_id'];
        if (!$this->validation->arrayKeysExists($this->request, $keys) ||
            !is_numeric($this->request['id']) ||
            !is_numeric($this->request['user_id']) ||
            !is_numeric($this->request['event_id'])
        ){
            throw new \Exception();
        }
    }


    function getEventUser()
    {
    }


    function getCommonData()
    {
    }


    function publicEvent()
    {
        $keys = ['id'];
        if (!$this->validation->arrayKeysExists($this->request, $keys) ||
            !is_numeric($this->request['id'])
        ){
            throw new \Exception();
        }
    }


    function getEventRecall()
    {
        $keys = ['id'];
        if (!$this->validation->arrayKeysExists($this->request, $keys) ||
            !is_numeric($this->request['id'])
        ){
            throw new \Exception();
        }
    }


    function sendEventRecall()
    {
        $keys = ['comment', 'event_id', 'likes'];
        if (!$this->validation->arrayKeysExists($this->request, $keys) ||
            !is_numeric($this->request['event_id']) ||
            !is_numeric($this->request['likes'])
        ){
            throw new \Exception();
        }
    }


    function complaint()
    {
        $keys = ['comment', 'event_id', 'type'];
        if (!$this->validation->arrayKeysExists($this->request, $keys) ||
            !is_numeric($this->request['event_id']) ||
            !is_numeric($this->request['type'])
        ){
            throw new \Exception();
            return;
        }
    }


    function getFollowerProfile()
    {
        $keys = ['user_id'];
        if (!$this->validation->arrayKeysExists($this->request, $keys) ||
            !is_numeric($this->request['user_id'])
        ){
            throw new \Exception();
        }
    }








}