<?php
/**
 * Created by PhpStorm.
 * UserVerification: rarog
 * Date: 02.08.2019
 * Time: 15:49
 */

namespace App\Controllers\User;


use App\Controllers\Base\BaseURIRequest;

interface URIUserRequest extends BaseURIRequest
{

    function getUser();

    function getFollowerProfile();

    function getEventUser();

    function getEventRecall();

    function sendEventRecall();

    function setUser();

    function setFilter();

    function subscribe();

    function unsubscribe();





    /**
     * Отказать юзеру в участии
     */
    function refuseParticipantEvent();

    /**
     * Подтвердить участие юзера
     */
    function acceptParticipant();

    function complaint();

    function participant();

    function unParticipant();


    /**
     * Создать мероприятие
     */
    function createEvent();

    /**
     * Изменить мероприятие
     */
    function updateEvent();

    /**
     * Отменить
     */
    function delEvent();

    /**
     * Мероприятия по фильтру и подпискам
     */
    function getEvents();

    function publicEvent();



    /**
     * Отправить сообщение
     */
    function sendMessage();

    function getTalks();

    function readTalk();




    function getCityByGeo();

    function getCityAutocomplete();

    function confirmPhone();

}