<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 28.11.2019
 * Time: 18:14
 */

namespace App\Controllers\Base;


interface BaseURIRequest
{
    public function handle();
}