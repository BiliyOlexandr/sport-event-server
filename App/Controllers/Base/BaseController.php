<?php

namespace App\Controllers\Base;


use App\Controllers\Injection;
use App\Globals\GlobalData;
use App\Model\User;
use App\Model\Utils\Response;
use App\Model\Utils\Util;
use App\Model\Utils\Validation;

/**
 * Class Controller
 * @package App\Controllers
 */
class BaseController
{

    protected $user;
    protected $TAG;
    protected $db;
	protected $token;
    protected $request = [];
    protected $action = ''; //Название метод для выполнения
    protected $response;
    protected $unit;
    protected $validation;

    /**
     * Controller constructor.
     * @param Injection $injection
     */
    public function __construct(Injection $injection){
        $this->action = $injection->action;
		$this->request = $injection->data;
        $this->token = $injection->token;
        $this->db = $injection->db;
        $this->unit = new Util();
        $this->validation = new Validation();
	}


    protected function getDataByLng(array $data, string $lng, array $rows = [])
    {
        $out = [];
        foreach ($data as &$d){
            $array = [
                'id'=>$d['id'],
                'name'=>$d[$lng],
                'icon'=>$d['icon']
            ];
            foreach ($rows as $r){
                if(isset($d[$r])){
                    $array[$r] = $d[$r];
                }
            }
            $out [] = $array;
        }
        return $out;
    }


    /**
     * @param $data
     */
    protected function setResponse(array $data) {
        $this->response= $data;
    }


    public function getResponse() : array
    {
	    return $this->response;
    }

    /**
     * @return string
     */
    public function getAction():string
    {
	    return $this->action;
    }


    /**
     * @return string
     */
    protected function generateToken() : string
    {
		return bin2hex(openssl_random_pseudo_bytes(25));
	}


    /**
     * @return string
     */
    protected function getIP() : string
    {
		if (isset($_SERVER['HTTP_CLIENT_IP'])){
			return $_SERVER['HTTP_CLIENT_IP'];
		} else if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])){
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else if (isset($_SERVER['REMOTE_ADDR'])){
            return $_SERVER['REMOTE_ADDR'];
		}
        return '';
	}


    /**
     * @param $table
     * @return bool
     */
    protected function hasAccept($table) : bool
    {
		if (!isset($this->token)){
            $this->setResponse(Response::badData('Token not found'));
			return false;
		}

        $jwt = explode(':', $this->token);
		if (count($jwt)!= 2){
            $this->setResponse(Response::badData('Bad token'));
		    return false;
        }

		$id = $jwt[0];
		$token = $jwt[1];
		if (!$this->validation->id($id)){
            $this->setResponse(Response::badData('ID not validation'));
		    return false;
        }

        switch ($table){
            case USER:
                $obj = User::Load($id);
                break;
        }


        if (!isset($obj)){
            $this->setResponse(Response::notFound('Not found'));
            return false;
        } else if ($obj->get('token') == $token){
		    $this->user = $obj;
		    return true;
		} else {
            $this->setResponse(Response::unauth('Bad token'));
            //LoggerBug::debug('4');
			return false;
		}
	}
	
	/*protected function searchDuplicate($table, $data){
		foreach($data as $key=>$value){
            $this->mySQL->where($key, $value, 'like');
		}
        return $this->mySQL->select(array('id'))->from($table)->where($data)->query();
	}*/

    /**
     * @param array $keys
     * @return array
     */
    protected function getObject(array $keys) : array
    {
        return array_sub($this->request, $keys);
	}

    /**
     * @param $keys
     * @param $obj
     * @return array
     */
    protected function createObject($keys, $obj) : array
    {
		$data = [];
		foreach ($keys as $key){
			if (isset($obj[$key])){
				$data[$key] = $obj[$key];
			}
		}		
		return $data;
	}


	protected function middleware(BaseMiddleware $baseMiddleware){
        $baseMiddleware->{$this->action}();
    }

}