<?php
/**
 * Created by PhpStorm.
 * UserVerification: rarog
 * Date: 02.08.2019
 * Time: 15:48
 */

namespace App\Controllers\Base;


use App\Model\Utils\Validation;

/**
 * Class BaseMiddleware
 * @package App\Controllers
 */
class BaseMiddleware{
    protected $validation;
    protected $request;


    /**
     * BaseMiddleware constructor.
     * @param Validation $validation
     * @param array|null $request
     */
    public function __construct(Validation $validation, array &$request)
    {
        $this->validation = $validation;
        $this->request = &$request;
    }


}