<?php
/**
 * Created by PhpStorm.
 * UserVerification: rarog
 * Date: 25.01.2019
 * Time: 15:38
 */
namespace App\Controllers;


use App\Globals\GlobalData;
use App\Library\DataBase\ROM\MySQL;

class Injection
{
    public $action;
    public $data=[];
    public $token;
    public $firebaseDriver;
    public $firebaseClient;
    public $db;

    public function __construct($action, array $data, $token=null)
    {
        $this->action = $action;
        foreach ($data as $key=>$val){
            if (!is_array($val)){
                $this->data[$key] = trim($val);
            } else {
                $this->data[$key] = $val;
            }
        }
        $GlobalData = new GlobalData();
        $dData = $GlobalData->getDb();
        $this->token = $token;
        $this->db = $GlobalData->getDb();
    }

}