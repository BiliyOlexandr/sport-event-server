<?php
/**
 * Created by PhpStorm.
 * UserVerification: rarog
 * Date: 18.05.2019
 * Time: 21:12
 */

namespace App\Library\DataBase;


class TableConfig
{
    const Int = FILTER_VALIDATE_INT;
    const Float = FILTER_VALIDATE_FLOAT;
    const Boolean = FILTER_VALIDATE_BOOLEAN;
    const String = 256;
    const Arr = 255;
    const Obj = 254;

    protected $default = [];
    protected $columns = [];
    protected $type = [];
    protected $table;

    public function __construct(array $fields, $table)
    {
        $this->columns = array_keys($fields);
        $this->table = $table;

        foreach ($this->columns as $COLUMN) {
            $this->default[$COLUMN] = $fields[$COLUMN][0];
            $this->type[$COLUMN] = $fields[$COLUMN][1];
        }
    }



    /**
     * @return array
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @return array
     */
    public function getTypes()
    {
        return $this->type;
    }

    /**
     * @param $key
     * @return string
     */
    public function getType($key)
    {
        return $this->type[$key];
    }

    /**
     * @return array
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * @return mixed
     */
    public function getTable()
    {
        return $this->table;
    }

}