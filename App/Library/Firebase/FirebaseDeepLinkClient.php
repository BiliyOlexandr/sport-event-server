<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 10.05.2019
 * Time: 19:43
 */

namespace App\Library\Firebase;


class FirebaseDeepLinkClient extends FirebaseDeepLink
{
    public static $FIREBASE_KEY_API = "AIzaSyDcuNxhbOSgr3Adl7O7L7KeW9n0KNMYW2s";
    public static $FIREBASE_CLIENT_URI  = "https://clientblitz.page.link";
    public static $FIREBASE_CLIENT_PLAY_MARKET  = "https://play.google.com/store/apps/details?id=by.blitz.client";
    public static $ANDROID_PACK_CLIENT  = "by.blitz.client";

    public static function Create(array $params){
        $obj = new FirebaseDeepLink(FirebaseDeepLinkClient::$FIREBASE_KEY_API,
            FirebaseDeepLinkClient::$FIREBASE_CLIENT_URI,
            FirebaseDeepLinkClient::$FIREBASE_CLIENT_PLAY_MARKET."&referral=".json_encode($params));
        //"https://taxiblitzby.atservers.net:7000?referral=".json_encode($params),

        $obj->setShort(true)
            ->setAndroidInfo(FirebaseDeepLinkClient::$ANDROID_PACK_CLIENT,
                FirebaseDeepLinkClient::$FIREBASE_CLIENT_PLAY_MARKET."&referral=".json_encode($params), 1);
        return $obj;
    }

}