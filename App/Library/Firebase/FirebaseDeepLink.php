<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 10.05.2019
 * Time: 17:36
 */

namespace App\Library\Firebase;


/**
 * Class FirebaseDeepLink
 * @package App\Library\Firebase
 */
class FirebaseDeepLink
{
    protected $apiKey = null;
    protected $URL = "https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=";
    protected $short = true;
    protected $request;


    /**
     * FirebaseDeepLink constructor.
     * @param $api_key
     * @param $uriPrefix
     * @param $link
     */
    public function __construct($api_key, $uriPrefix, $link)
    {
        $this->apiKey = $api_key;
        $this->URL = $this->URL.$api_key;
        $this->request = [
            'dynamicLinkInfo'=> [
                "domainUriPrefix"=>  $uriPrefix,
                "link"=> $link,
            ],
        ];
        return $this;
    }

    /**
     * @param $package
     * @param string $fallBack
     * @param string $minPack
     * @return $this
     */
    public function setAndroidInfo($package, $fallBack='', $minPack = '1'){
        $this->request['dynamicLinkInfo']["androidInfo"] = [
            "androidPackageName"=> $package,
            "androidFallbackLink" => $fallBack,
            "androidMinPackageVersionCode" => strval($minPack)
        ];
        return $this;
    }


    /**
     * @param $iosBundleId
     * @param $iosFallbackLink
     * @param $iosCustomScheme
     * @param $iosIpadFallbackLink
     * @param $iosIpadBundleId
     * @param $iosAppStoreId
     * @return $this
     */
    public function setIosInfo($iosBundleId, $iosFallbackLink, $iosCustomScheme, $iosIpadFallbackLink, $iosIpadBundleId, $iosAppStoreId){
        $this->request['dynamicLinkInfo']['iosInfo'] = [
            "iosBundleId"       => strval($iosBundleId),
            "iosFallbackLink"   => $iosFallbackLink,
            "iosCustomScheme"   => $iosCustomScheme,
            "iosIpadFallbackLink"=> $iosIpadFallbackLink,
            "iosIpadBundleId"   => strval($iosIpadBundleId),
            "iosAppStoreId"     => strval($iosAppStoreId)
        ];
        return $this;
    }

    /**
     * @param $short
     * @return $this
     */
    public function setShort($short)
    {
        $this->short = $short;
        return $this;
    }


    /**
     * @return bool|mixed
     */
    function generate(){
        if ($this->short){
            $request['suffix']= ["option"=> "SHORT"];
        } else {
            $request['suffix']= ["option"=> "UNGUESSABLE"];
        }

        $aHeaders = [
            'Authorization: key='.$this->apiKey,
            'Content-Type: application/json'
        ];

        print_log(__FILE__, __LINE__,json_encode($this->request)."\n");
        
        // Curl request
        $ch				= curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->URL);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $aHeaders);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($this->request));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result 		= curl_exec($ch);
        $httpcode		= curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        print_log(__FILE__, __LINE__,$result);

        if ($httpcode == "401") { // unauthorized
            return false;
        } else if ($httpcode == "200") { // OK
            if ($this->short){
                return json_decode($result, true)['shortLink'];
            } else {
                return json_decode($result, true);
            }
        } else { // Unkown
            return false;
        }
    }


/*
{"dynamicLinkInfo": {
"domainUriPrefix": string,
"link": string,

"androidInfo": {
"androidPackageName": string,
"androidFallbackLink": string,
"androidMinPackageVersionCode": string
},

"iosInfo": {
    "iosBundleId": string,
      "iosFallbackLink": string,
      "iosCustomScheme": string,
      "iosIpadFallbackLink": string,
      "iosIpadBundleId": string,
      "iosAppStoreId": string
    },

    "navigationInfo": {
    "enableForcedRedirect": boolean,
    },

    "analyticsInfo": {
    "googlePlayAnalytics": {
        "utmSource": string,
        "utmMedium": string,
        "utmCampaign": string,
        "utmTerm": string,
        "utmContent": string,
        "gclid": string
      },
      "itunesConnectAnalytics": {
        "at": string,
        "ct": string,
        "mt": string,
        "pt": string
      }
    },
    "socialMetaTagInfo": {
    "socialTitle": string,
      "socialDescription": string,
      "socialImageLink": string
    }
  },
  "suffix": {
    "option": "SHORT" or "UNGUESSABLE"
  }}
*/

}