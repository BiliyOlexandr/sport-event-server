<?php /** @noinspection ALL */
/** @noinspection PhpInconsistentReturnPointsInspection */
/** @noinspection PhpInconsistentReturnPointsInspection */

/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 02.02.2019
 * Time: 23:46
 */
namespace App\Library\Firebase;
use App\Library\Logger\LoggerBug;

class FirebaseData extends  FCMPushNotification
{
    public function __construct($key)
    {
        try {
            parent::__construct($key);
        } catch (FCMPushNotificationException $e) {
            LoggerBug::error('FirebaseData', $e);
        }
    }

    public function sendData($aRegistrationTokens, array $data)
    {
        $option = [
            'time_to_live' => 30,
            'priority' => 'high'
        ];


        $aPayload = [
            'data' => $data
        ];

        if (is_array($aRegistrationTokens)){
            $list = [];
            foreach ($aRegistrationTokens as $idDev){
                $list[] = $idDev;
            }
            try {
                $res = parent::sendToDevices($list, $aPayload, $option);
            } catch (FCMPushNotificationException $e) {
            }
        } else {
            try {
                $res = parent::sendToDevice($aRegistrationTokens, $aPayload, $option);
            } catch (FCMPushNotificationException $e) {
                LoggerBug::error('Driver FCM', $e);
            }
        }

        print_log(__FILE__, __LINE__,"Send-".json_encode($res)."\n");
        return $res;

    }



}