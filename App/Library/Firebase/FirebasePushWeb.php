<?php
/**
 * Created by PhpStorm.
 * UserVerification: rarog
 * Date: 17.08.2019
 * Time: 18:45
 */

namespace App\Library\Firebase;


use Exception;

class FirebasePushWeb
{

    protected $userToken;
    protected $title;
    protected $data;
    protected $body;
    protected $url_action;
    protected $url_icon = 'https://eralash.ru.rsz.io/sites/all/themes/eralash_v5/logo.png?width=192&height=192';


    public function __construct($userToken)
    {
        $this->userToken = $userToken;
    }

    /**
     * @param $userToken
     * @return FirebasePushWeb
     * @throws Exception
     */
    public static function Create($userToken){
        $obj = new FirebasePushWeb($userToken);
        return $obj;
    }

    /**
     * @param $title
     * @return $this
     */
    public function setTitle($title){
        $this->title = $title;
        return $this;
    }


    /**
     * @param $body
     * @return $this
     */
    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }

    /**
     * @param mixed $url_action
     * @return FirebasePushWeb
     */
    public function setUrlAction($url_action)
    {
        $this->url_action = $url_action;
        return $this;
    }

    /**
     * @param string $url_icon
     * @return FirebasePushWeb
     */
    public function setUrlIcon($url_icon)
    {
        $this->url_icon = $url_icon;
        return $this;
    }



    public function send(){

        if ( isset($this->title) && isset($this->body) ){
            $notify = [
                'title' => $this->title,
                'body' => $this->body,
                'icon' => $this->url_icon,
            ];

            if ( isset($this->url_action) ){
                $notify['click_action'] = $this->url_action;
            }

            $request_body['notification'] = $notify;
        }

        if (isset($this->data)){
            $request_body['data'] = $this->data;
        }

        if (empty($request_body)){
            return false;
        }
        $request_body['time_to_live'] = 60;
        $request_body['priority'] = 'high';
        $request_body['to'] =  $this->userToken;
        //for ios
        //$request_body['content_available'] = true;
        //$request_body['priority'] ;



        $request_headers = array(
            'Content-Type: application/json',
            'Authorization: key=' . FIRREBASE_API_KEY,
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, FIREBASE_URL);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request_body));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $response = curl_exec($ch);
        $httpcode		= curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($httpcode == "401") { // unauthorized
            return false;
        } else if ($httpcode == "200") { // OK
            return json_decode($response, true);
        }

        return $response;
    }

    /*
     * {
  "message": {
    "token" : <token of destination app>,
    "notification": {
      "title": "FCM Message",
      "body": "This is a message from FCM"
    },
    "webpush": {
      "headers": {
        "Urgency": "high"
      },
      "notification": {
        "body": "This is a message from FCM to web",
        "requireInteraction": "true",
        "badge": "/badge-icon.png"
      }
    }
  }
}
     * */

}