<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 23.01.2019
 * Time: 1:58
 */
namespace App\Library\Firebase;
use Exception;

/**
 * Class FCMPushNotificationException
 * @package App\Library\Firebase
 */
class FCMPushNotificationException extends Exception
{

}