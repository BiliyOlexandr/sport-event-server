<?php



/**
 * Created by PhpStorm.
 * UserVerification: rarog
 * Date: 01.08.2019
 * Time: 16:33
 * @param $data
 * @param $keys
 * @return array
 */


function array_sub(array $data, array $keys){
    $res = [];
    foreach ($keys as $key){
        if (isset($data[$key])){
            $res[$key] = $data[$key];
        }
    }
    return $res;
}

/**
 * @param array $data
 * @param array $val
 * @return array
 */
function array_search_by_value (array $data, array $val)
{
    foreach ($data as $d){
        if (is_array($d) && array_search($val, $d) !== false){
            return $d;
        }
    }
    return [];
}

function indexOf(array $data, $key, $val, $substr = false){
    foreach ($data as $index => $d){
        if ($substr){
            if (strpos($d[$key], $val) !== false || strpos($val, $d[$key]) !== false ){
                return $index;
            }
        } else {
            if ($d[$key] == $val){
                return $index;
            }
        }
    }
    return -1;
}



spl_autoload_register(
    function ($class) {
        $class =  str_replace('\\', '/', $class) . '.php';
        ///print_r($class);
        //echo $class;
        if (file_exists(DIR .$class) == 1) {
            require_once DIR . $class;
        }
    }
);


function print_log($file, $line, $data){
    //if (GlobalData::$mode){return;}
    if (is_array($data) || is_object($data) || is_bool($data)){
        $data = json_encode($data);
    }

    print_r("\n".str_replace(DIR, '', $file).' - '.$line.' : '.$data."\n");

}


function isset_columns(array $data, array $keys, array $defaultValue){
    $res = [];
    foreach ($keys as $key) {
        //if ($column == 'id'){continue;}
        if (!isset($data[$key])){
            $res[$key] = $defaultValue[$key];
        } else {
            $res[$key] = $data[$key];
        }
    }
    return $res;
}
