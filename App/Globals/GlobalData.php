<?php
/**
 * Created by PhpStorm.
 * UserVerification: rarog
 * Date: 03.08.2019
 * Time: 2:56
 */

namespace App\Globals;

use App\Library\DataBase\ROM\MySQL;

use Exception;


class GlobalData
{
    protected static $instance;
    public static function getInstance()
    {
        return self::$instance;
    }


    public static $timeZone = 0;
    /**
     * @var boolean, false-debug
     */
    public static $mode = false;

    /**
     * @var boolean
     */
    public static $isTestServer = false;

    protected $properties;
    protected $db;
    protected $sportKind;
    protected $countries;
    protected $typeParticipant;
    protected $typeEvent;

    public function __construct()
    {
        self::$instance = $this;
        $this->db = new MySQL();
        $this->sportKind = $this->db->get(SPORT_KIND);
        $this->countries = $this->db->get(COUNTRY);
        $this->typeParticipant = $this->db->get(TYPE_PARTICIPANT);
        $this->typeEvent = $this->db->get(TYPE_EVENT);
    }

    public static function Create() : self
    {
       $obj = new GlobalData();
       return $obj;
    }


    /**
     * @return MySQL
     */
    public function getDb() :MySQL
    {
        //reconnect
        try {
            if (!$this->db->ping()) {
                //$this->db->connect();
                $this->db = new MySQL();
            }
        } catch (Exception $exception) {
            $this->db = new MySQL();
        }
        return $this->db;
    }


    /**
     * @param $table
     * @return array | null
     */
    public function &getTable($table){
        //print_log(__FILE__, __LINE__, $table);
        switch ($table){
            case SPORT_KIND:
                return $this->sportKind;

            case TYPE_EVENT:
                return $this->typeEvent;

            case TYPE_PARTICIPANT:
                return $this->typeParticipant;

        }
        return null;
    }

    /**
     * @return array
     */
    public function getSportKind() : array
    {
        return $this->sportKind;
    }


    /**
     * @return array
     */
    public function getCountries() : array
    {
        return $this->countries;
    }


    /**
     * @return array
     */
    public function getTypeParticipant()
    {
        return $this->typeParticipant;
    }


    /**
     * @return array|\MysqliDb
     */
    public function getTypeEvent()
    {
        return $this->typeEvent;
    }
}