<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 16.11.2019
 * Time: 17:22
 */

namespace App\Globals;


use App\Library\DataBase\ROM\MySQL;
use Workerman\Worker;

class MainGlobalData
{
    /**
     * @var boolean, false-debug
     */
    public static $mode = false;
    /**
     * @var boolean
     */
    public static $isTestServer = false;
    protected $db;
    protected $tcpClient;

    public function __construct()
    {
        $this->db = new MySQL();
    }

    /**
     * @return MySQL
     */
    public function getDb() : MySQL
    {
        return $this->db;
    }

    /**
     * @return mixed
     */
    public function getTcpClient()
    {
        return $this->tcpClient;
    }

    /**
     * @param mixed $tcpClient
     */
    public function setTcpClient(Worker $tcpClient)
    {
        $this->tcpClient = $tcpClient;
    }



}