<?php

require_once('/home/MyCo/web/App/Config/app.php');

require_once(DIR.'App/Config/Tables.php');
require_once(DIR.'App/Library/Variable/bootstrap.php');

require DIR.'vendor/autoload.php';

use App\Controllers\BotController;
use App\Controllers\UserController;
use App\Controllers\DriverController;
use App\Controllers\Injection;
use App\Controllers\Registration\MainConnectorController;
use App\Controllers\SuperAdminController;
use App\Globals\GlobalData;
use App\Globals\GlobalDataAccount;
use App\Library\Logger\LoggerBug;
use App\Library\Logger\LoggerRequest;
use App\Globals;
use App\Model\Handler\MyTask;
use App\Model\Response;
use Workerman\Connection\AsyncTcpConnection;
use Workerman\Connection\ConnectionInterface;
use Workerman\Worker;





// #### http worker ####
Worker::$eventLoopClass = '\Workerman\Events\Ev';
Worker::$daemonize = GlobalDataAccount::$mode = false;
Worker::$stdoutFile = '/tmp/stdout.log';

$worker = new Worker('http://'.URL_ACCOUNT.":".SOCKET_ACCOUNT);


// 1 processes
$worker->count = 1;


$worker->onBufferFull = function()
{
    LoggerBug::debug('Buffer full');
};

$worker->onBufferDrain = function()
{
   LoggerBug::debug('Buffer drain');

};

$worker->onWorkerStart = function()
{
    GlobalDataAccount::Create();
    GlobalDataAccount::$isTestServer = true;


    // Websocket protocol for client.
    //AsyncTcpConnection (tcp/ws/text/frame etc...)
    $ws_connection = new AsyncTcpConnection("ws://echo.websocket.org:80");
    $ws_connection->onConnect = function($connection){
        $connection->send('hello');
    };
    $ws_connection->onMessage = function($connection, $data){
        echo "recv: $data\n";
    };
    $ws_connection->onError = function($connection, $code, $msg){
        echo "error: $msg\n";
    };
    $ws_connection->onClose = function($connection){
        echo "connection closed\n";
    };
    $ws_connection->connect();


    //MyTask::getInstance()->run();
    //Worker::$globalEvent->add(SIGALRM, EventInterface::EV_SIGNAL, function($signal)
    //{
    //    print_log(__FILE__, __LINE__,$signal);
    //});
};

$worker->onMessage = function(ConnectionInterface $connection, $data) {

    set_error_handler(function ($errno, $errstr, $errfile, $errline ) {
        $exception = new ErrorException($errstr, $errno, 0, $errfile, $errline);
        LoggerBug::error('Error App', $exception);
    });

    error_reporting(E_ALL);
    $time_start =  microtime(true)*1000;


    print_log(__FILE__, __LINE__,"requestUri-".$_SERVER['REQUEST_URI']);

    $requestUri = explode('/', trim($_SERVER['REQUEST_URI'],'/'));
    $response = [];
    if ($requestUri[0] === 'api'){
        $controller = $requestUri[1];
        $action = $requestUri[2];
        $api = null;
        $injection = new Injection(GlobalDataAccount::getInstance()->getDb(), $action, $_REQUEST);
        $api = MainConnectorController::Run($injection);
        $response = $api->getResponse();

    } else if ($requestUri[0] === 'bot'){
        array_shift($requestUri);
        $bot = array_shift($requestUri);
        print_log(__FILE__, __LINE__,$_REQUEST);
        BotController::Run($bot, $_REQUEST);
        $response = 1;
    }

    $connection->send(json_encode($response));//JSON_NUMERIC_CHECK
};


Worker::runAll();


