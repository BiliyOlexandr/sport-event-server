<?php
/**
 * Created by PhpStorm.
 * UserVerification: rarog
 * Date: 20.08.2019
 * Time: 22:09
 */
/*
function saveFile(){
    $data = "\n"."\n".'--'.date('Y-m-d H:i:s').'--'."\n";
    $data .= 'post1 - ' .json_encode($_POST)."\n";
    $data .= 'post2 - ' .file_get_contents("php://input")."\n";
    file_put_contents('mylog.txt', $data, FILE_APPEND);
}

saveFile();
echo 1;
return;*/

$data = json_decode(file_get_contents("php://input"), true);
$url = 'http://itineris.cf:7001/bot/viber/';
//$url = 'http://itineris.cf:7002/telegram';

$send = [];

if (isset($data['event'])){
    if ($data['event'] === 'message'){
        //confirm phone
        if (isset($data['message']['contact'])){
            if (count($data['message']['contact']) === 1){
                $send = [
                    'user' => ['id'=>$data['sender']['id'], 'name'=>$data['sender']['name']],
                    'phone_number' => $data['message']['contact']['phone_number'],
                    'event' => 'confirm_phone',
                    'text' => $data['message']['text']
                ];
            } else {
                //send other contact
                return;
            }
            //confirm inner/registration
        } else if (strpos($data['message']['text'], '_') !== false){
            $send = [
                'user' => ['id'=>$data['sender']['id'], 'name'=>$data['sender']['name']],
                'event' => 'confirm_inner',
                'text' => $data['message']['text']
            ];
        } else {
            return;
        }
    } else if ($data['event'] === 'subscribed'){
        $send = [
            'user' => ['id'=>$data['sender']['id']],
            'event' => 'subscribed'
        ];
        //{"event":"unsubscribed","timestamp":1572191210605,"chat_hostname":"SN-90_","user_id":"LGPX7Ickm5LWFSXMSIIyWg==","message_token":5369268784763831790}
    } else if ($data['event'] === "notSubscribed") {
        $send = [
            'user' => ['id'=>$data['user_id']],
            'event' => 'unsubscribed',
        ];
    } else {
        return;
    }
} else {
    return;
}

$url .= '?'. http_build_query($send);
//echo $url;
//return;

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($send));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

$response = curl_exec($ch);
curl_close($ch);

/*
 * confirm inner
 {"event":"message","timestamp":1572190980825,"chat_hostname":"SN-CHAT-10_","message_token":5369267821336397553,
"sender":{"id":"LGPX7Ickm5LWFSXMSIIyWg==","name":"\u0421\u0435\u0440\u0433\u0435\u0439","language":"ru","country":"UA","api_version":8},
"message":{"text":"confirm_inner_phone","type":"text"},
"silent":false}

 *
 * response phone - required phone
 {"event":"message","timestamp":1572190710951,"chat_hostname":"SN-CHAT-10_","message_token":5369266689935476238,
"sender":{"id":"LGPX7Ickm5LWFSXMSIIyWg==","name":"\u0421\u0435\u0440\u0433\u0435\u0439","language":"ru","country":"UA","api_version":8},
"message":{"text":"confirm_phone","type":"contact","contact":{"phone_number":"380688888514"}},
"silent":false}


//send other contact
{"event":"message","timestamp":1572189845560,"chat_hostname":"SN-CHAT-10_","message_token":5369263059681478344,
"sender":{"id":"LGPX7Ickm5LWFSXMSIIyWg==","name":"\u0421\u0435\u0440\u0433\u0435\u0439","language":"ru","country":"UA","api_version":8},
"message":{"text":"\u041A\u0418\u0421\u042F \u041C +380954848304","type":"contact",
"contact":{"name":"\u041A\u0418\u0421\u042F \u041C","phone_number":"+380954848304","avatar":"https://media-direct.cdn.viber.com/download_photo?dlid=0-03-04-a1f26c325854bd94c1c2f130b95f0966e7d10d18294998b07d39af37b6a202f6&fltp=jpg&imsz=0000"}},
"silent":false}



//registration
{"event":"subscribed","timestamp":1572186082236,"chat_hostname":"SN-90_",
"user":{"id":"LGPX7Ickm5LWFSXMSIIyWg==","name":"РЎРµСЂРіРµР№","language":"ru","country":"UA","api_version":8},
"message_token":5369247274825221737}

//registartion by deeplink
{"event":"message","timestamp":1572195017077,"chat_hostname":"SN-CHAT-10_","message_token":5369284750642055004,
"sender":{"id":"LGPX7Ickm5LWFSXMSIIyWg==","name":"\u0421\u0435\u0440\u0433\u0435\u0439","language":"ru","country":"UA","api_version":8},
"message":{"text":"Confirm 380688888514","type":"text"},"silent":false}


//location
{"event":"message","timestamp":1572183816549,"chat_hostname":"SN-CHAT-10_","message_token":5369237772159747650,
"sender":{"id":"LGPX7Ickm5LWFSXMSIIyWg==","name":"\u0421\u0435\u0440\u0433\u0435\u0439","language":"ru","country":"UA","api_version":8},
"message":{"type":"location","location":{"lat":49.956289,"lon":36.28099}},"silent":false}


//unsubscribe - иногда не доходит
{"event":"unsubscribed","timestamp":1572191210605,"chat_hostname":"SN-90_","user_id":"LGPX7Ickm5LWFSXMSIIyWg==","message_token":5369268784763831790}



//subscribe
{"event":"subscribed","timestamp":1572188468502,"chat_hostname":"SN-90_",
"user":{"id":"LGPX7Ickm5LWFSXMSIIyWg==","name":"РЎРµСЂРіРµР№","language":"ru","country":"UA","api_version":8},
"message_token":5369257283550250969}


 * */