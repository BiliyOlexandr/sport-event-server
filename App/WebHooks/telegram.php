<?php
/**
 * Created by PhpStorm.
 * UserVerification: rarog
 * Date: 20.08.2019
 * Time: 22:09
 *//*
function saveFile(){
    $data = "\n"."\n".'--'.date('Y-m-d H:i:s').'--'."\n";
    $data .= 'post1 - ' .json_encode($_POST)."\n";
    $data .= 'post2 - ' .file_get_contents("php://input")."\n";
    file_put_contents('mylog.txt', $data, FILE_APPEND);
}

saveFile();
echo 1;
return;*/


$data = json_decode(file_get_contents("php://input"), true);
$url = 'http://itineris.cf:7001/bot/telegram/';
//$url = 'http://itineris.cf:7002/telegram';

$send = [];
if (isset($data['message'])){
    $send = [
        'status'=>200,
        'from'=>$data['message']['from'],
        'text'=>$data['message']['text']
    ];
    if (isset($data['message']['contact'])){
        $send['phone_number'] = $data['message']['contact']['phone_number'];
    }
} else {
    return;
}

$url .= '?'. http_build_query($send);
//echo $url;
//return;

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($send));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

$response = curl_exec($ch);
//$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
curl_close($ch);

/*
 * response phone
 {"update_id":260577755,
"message":{"message_id":67,"from":{"id":388204446,"is_bot":false,"first_name":"Sergey","last_name":"Rarog","username":"RarogSV","language_code":"ru"},
"chat":{"id":388204446,"first_name":"Sergey","last_name":"Rarog","username":"RarogSV","type":"private"},"date":1570375675,
"reply_to_message":{"message_id":66,"from":{"id":788713045,"is_bot":true,"first_name":"ItinerisBot","username":"IT_INERIS_BOT"},
"chat":{"id":388204446,"first_name":"Sergey","last_name":"Rarog","username":"RarogSV","type":"private"},"date":1570375577,"text":"Phone"},
"contact":{"phone_number":"380688888514","first_name":"Sergey","last_name":"Rarog","user_id":388204446}}}


//registration
{"update_id":260577763,
"message":{"message_id":83,"from":{"id":388204446,"is_bot":false,"first_name":"Sergey","last_name":"Rarog","username":"RarogSV","language_code":"ru"},
"chat":{"id":388204446,"first_name":"Sergey","last_name":"Rarog","username":"RarogSV","type":"private"},"date":1570882690,"text":"/start _380688888514",
"entities":[{"offset":0,"length":6,"type":"bot_command"}]}}


{"update_id":260577764,
"message":{"message_id":84,"from":{"id":482938962,"is_bot":false,"first_name":"Irina","last_name":"Rarog","username":"Irina_Rarog"},
"chat":{"id":482938962,"first_name":"Irina","last_name":"Rarog","username":"Irina_Rarog","type":"private"},"date":1570883082,"text":"/start _380688888514",
"entities":[{"offset":0,"length":6,"type":"bot_command"}]}}
 * */