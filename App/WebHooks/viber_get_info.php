<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 27.10.2019
 * Time: 14:05
 */

$url = "https://chatapi.viber.com/pa/get_account_info";
$send = [];

$curld = curl_init();
curl_setopt($curld, CURLOPT_HTTPHEADER, ['X-Viber-Auth-Token: 4a8359ab5a27d2b8-a738fef597a52c8a-38a42666b9bd80cf']);
curl_setopt($curld, CURLOPT_POST, true);
curl_setopt($curld, CURLOPT_POSTFIELDS, []);
curl_setopt($curld, CURLOPT_URL, $url);
curl_setopt($curld, CURLOPT_RETURNTRANSFER, true);

echo curl_exec($curld);
curl_close ($curld);
