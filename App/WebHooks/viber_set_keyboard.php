<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 27.10.2019
 * Time: 14:05
 */

$url = "https://chatapi.viber.com/pa/send_message";
//confirm phone
$send = [
   "receiver"=>"LGPX7Ickm5LWFSXMSIIyWg==",
   "min_api_version"=>3,
   "type"=>"text",
   "text"=>"We need your phone number",
   "keyboard"=>[
        "Type"=>"keyboard",
        "DefaultHeight"=>true,
        "Buttons"=>[
           [
              "ActionType"=>"share-phone",
              "ActionBody"=>"confirm_phone",
              "Text"=>"Send Contact", // text button
              "TextSize"=>"regular"
           ]
        ]
   ]
];
$send = ["text"=>"Подтвердите ваш номер телефона.",
    "keyboard"=>["Type"=>"keyboard","DefaultHeight"=>true,"Buttons"=>[["ActionType"=>"share-phone","ActionBody"=>"confirm_phone","Text"=>"Подтвердить","TextSize"=>"regular"]]],
"receiver"=>"LGPX7Ickm5LWFSXMSIIyWg==","min_api_version"=>3,"type"=>"text"];


//confirm inner
/*$send = [
    "receiver"=>"LGPX7Ickm5LWFSXMSIIyWg==",
    "min_api_version"=>3,
    "type"=>"text",
    "text"=>"Confirm inner",
    "keyboard"=>[
        "Type"=>"keyboard",
        "DefaultHeight"=>true,
        "Buttons"=>[
            [
                "ActionType"=>"reply",
                "ActionBody"=>"confirm_inner_phone",
                "Text"=>"Send Contact", // text button
                "TextSize"=>"regular"
            ]
        ]
    ]
];*/

$curld = curl_init();
curl_setopt($curld, CURLOPT_HTTPHEADER, ['X-Viber-Auth-Token: 4a8359ab5a27d2b8-a738fef597a52c8a-38a42666b9bd80cf']);
curl_setopt($curld, CURLOPT_POST, true);
curl_setopt($curld, CURLOPT_POSTFIELDS, json_encode($send));
curl_setopt($curld, CURLOPT_URL, $url);
curl_setopt($curld, CURLOPT_RETURNTRANSFER, true);

echo curl_exec($curld);
curl_close ($curld);


/**
{"event":"message","timestamp":1572186309036,"chat_hostname":"SN-CHAT-10_","message_token":5369248226428913592,
 * "sender":{"id":"LGPX7Ickm5LWFSXMSIIyWg==","name":"\u0421\u0435\u0440\u0433\u0435\u0439","language":"ru","country":"UA","api_version":8},
 * "message":{"text":"reply to me","type":"text"},"silent":false}
 *
 * //отписался
 * {"status":6,"status_message":"notSubscribed","message_token":5369256946256204919,"chat_hostname":"SN-CHAT-06_"}

 * //ok
 * {"status":0,"status_message":"ok","message_token":5369257536034065065,"chat_hostname":"SN-CHAT-03_"}
 */