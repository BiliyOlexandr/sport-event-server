<?php

namespace App\Model;


use App\Library\DataBase\TableConfig;
use App\Model\Base\ObjectBaseDB;
use App\Model\Interfaces\Creator;

/**
 * Class UserVerification
 * @package App\Model
 */
class Order extends ObjectBaseDB implements Creator
{
    function __construct(){
        $keys = [
            'id'            => [-1, TableConfig::Int],
            'user_id'       => [-1, TableConfig::Int],
            'event_id'       => [-1, TableConfig::Int],
            'status'       => [1, TableConfig::Int],
            'accept'       => [0, TableConfig::Int],
            'created'       => [-1, TableConfig::Int]
        ];

        parent::__construct(new TableConfig($keys, ORDERS));
    }

    public static function Load($id)
    {
        $obj = new Order();
        $obj->_load($id);
        return $obj;
    }
    public static function Create(array $data)
    {
        $obj = new Order();
        $obj->toObject($obj->castType($obj->issetColumns($data)));
        return $obj;
    }
    public static function Search(array $data)
    {
        $obj = new Order();
        $obj->loadLike($data);
        return $obj;
    }


}
