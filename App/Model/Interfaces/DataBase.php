<?php
namespace App\Model\Interfaces;
interface DataBase{

	public function update(array $data);
	public function insert();
	public function delete();
	public function load($id);
	public function loadLikeDB($data);
}
