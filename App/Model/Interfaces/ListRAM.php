<?php
/**
 * Created by PhpStorm.
 * UserVerification: rarog
 * Date: 25.02.2019
 * Time: 17:01
 */

namespace App\Model\Interfaces;
use App\Library\DataBase\ROM\MySQL;


/**
 * Interface ListRAM
 * @package App\Model\Interfaces
 */
interface ListRAM
{
    /**
     * @param array $obj
     * @param MySQL $mySQL
     * @return array|boolean
     */
    public function insert(array $obj, MySQL $mySQL);

    /**
     * @param array $data
     * @param $id
     * @param MySQL $mySQL
     * @return mixed
     */
    public function update(array $data, $id, MySQL $mySQL);

    /**
     * @param $id
     * @param null $row
     * @return mixed
     */
    public function select($id, $row=null);

    /**
     * @param array $data
     * @param null $row
     * @return mixed
     */
    public function searchBy(array $data, $row=null);

    /**
     * @param array $data
     * @param null $row
     * @return mixed
     */
    public function searchObjectBy(array $data, $row=null);

    /**
     * @param $id
     * @param MySQL $mySQL
     * @return mixed
     */
    public function del($id, MySQL $mySQL);

    /**
     * @param array $idList
     * @param MySQL $mySQL
     * @return mixed
     */
    public function delList(array $idList, MySQL $mySQL);
    //public function updateList(array $data, $index);

    public function getAll();
}