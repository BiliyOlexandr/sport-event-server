<?php
/**
 * Created by PhpStorm.
 * UserVerification: rarog
 * Date: 17.01.2019
 * Time: 12:26
 */
namespace App\Model\Interfaces;




interface Creator
{
    public static function Load($id);
    public static function Create(array $data);
    public static function Search(array $data);
}