<?php

namespace App\Model;


use App\Library\DataBase\TableConfig;
use App\Model\Base\ObjectBaseDB;
use App\Model\Interfaces\Creator;
use Kreait\Firebase\Exception\FirebaseException;
use Kreait\Firebase\Exception\MessagingException;
use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;

/**
 * Class UserVerification
 * @package App\Model
 */
class User extends ObjectBaseDB implements Creator
{

    const NEW_MESSAGE = 'new_message';
    const NEW_PARTICIPANT = 'new_participant';
    const DENIED_PARTICIPANT = 'denied_participant';

    function __construct(){
        $keys = [
            'id'            => [-1, TableConfig::Int],
            'name'          => ['', TableConfig::String],
            'phone'         => ['', TableConfig::String],
            'email'         => ['', TableConfig::String],
            'pass'          => ['', TableConfig::String],
            'country_id'    => [-1, TableConfig::Int],
            'city_id'       => [-1, TableConfig::Int],
            'token'         => ['', TableConfig::String],
            'lang'          => ['RU', TableConfig::String],
            'is_command'    => [0, TableConfig::Int],
            'event_active'  => [0, TableConfig::Int],
            'gender'        => [0, TableConfig::Int],
            'created'       => [-1, TableConfig::Int],
            'icon'          => ['', TableConfig::String],
            'fb_token'      => ['', TableConfig::String],
            'vk_token'      => ['', TableConfig::String],
            'filter'        => [Filter::Create()->toString(), TableConfig::String],

            'firebase_token_mobile'=> ['', TableConfig::String],
            'firebase_token_web'=> ['', TableConfig::String],
        ];

        parent::__construct(new TableConfig($keys, USER));
    }

    public static function Load($id) : self
    {
        $obj = new User();
        $obj->_load($id);
        return $obj;
    }
    public static function Create(array $data) : self
    {
        $obj = new User();
        $obj->toObject($obj->castType($obj->issetColumns($data)));
        return $obj;
    }
    public static function Search(array $data) : self
    {
        $obj = new User();
        $obj->loadLike($data);
        return $obj;
    }

    public function sendMessage(array $notification = [], array $data = [])
    {
        $tokens = [];

        if (strlen($this->data['firebase_token_web']) > 0){
            $tokens[] = $this->data['firebase_token_web'];
        }
        if (strlen($this->data['firebase_token_mobile']) > 0){
            $tokens[] = $this->data['firebase_token_mobile'];
        }
        if (count($tokens) == 0){
            return;
        }
        User::sendMessages($tokens, $notification, $data);
    }


    public static function sendMessages(array $deviceTokens, array $notification = [], array $data = [])
    {
        $message = CloudMessage::new();

        if (count($notification) > 0){
            $notification = Notification::fromArray($notification);
            $message->withNotification($notification); // optional
        }

        if (count($data) > 0){
            $message->withData($data);
        }

        $factory = (new Factory())->withServiceAccount(\Kreait\Firebase\ServiceAccount::fromJsonFile(DIR.'App/Config/firebase_credentials.json'));
        $messaging = $factory->createMessaging();

        try {
            $messaging->sendMulticast($message, $deviceTokens);
        } catch (MessagingException $e) {
        } catch (FirebaseException $e) {
        }
    }



}
