<?php
/**
 * Created by PhpStorm.
 * UserVerification: rarog
 * Date: 04.08.2019
 * Time: 17:54
 */

namespace App\Model\Utils;



class Response
{
    const STATUS_OK = 200;
    const STATUS_BAD_REQUEST = 400;
    const STATUS_UNAUTHORIZED = 401;
    const STATUS_PAYMENT = 402;
    const STATUS_NOT_FOUND = 404;
    const STATUS_PHONE_NOT_FOUND = 405;
    const STATUS_DUPLICATE = 406;
    const STATUS_URI = 407;
    const STATUS_BAD_IMAGE = 408;
    const STATUS_GONE = 410;
    const STATUS_OLD_DATA = 455;
    const STATUS_LOCKED = 423;
    const STATUS_ERROR = 500;



    public static function badURI($comment=null){
        return self::badResponse(Response::STATUS_BAD_REQUEST, $comment);
    }


    public static function create(array $data = []){
        $data['status'] = Response::STATUS_OK;
        return $data;
    }

    public static function goneData($comment=null, $addon=null){
        return self::badResponse(Response::STATUS_GONE, $comment, $addon);
    }

    public static function oldData($comment=null, $addon=null){
        return self::badResponse(Response::STATUS_OLD_DATA, $comment, $addon);
    }

    public static function badData($comment=null, $addon=null){
        return self::badResponse(Response::STATUS_BAD_REQUEST, $comment, $addon);
    }


    public static function unauth($comment=null, $addon=null){
        return self::badResponse(Response::STATUS_UNAUTHORIZED, $comment, $addon);
    }

    public static function payment($comment=null, $addon=null){
        return self::badResponse(Response::STATUS_PAYMENT, $comment, $addon);
    }

    public static function notFound($comment=null, $addon=null){
        return self::badResponse(Response::STATUS_NOT_FOUND, $comment, $addon);
    }

    public static function notFoundPhone($comment=null, $addon=null){
        return self::badResponse(Response::STATUS_PHONE_NOT_FOUND, $comment, $addon);
    }

    public static function duplicate($comment=null, $addon=null){
        return self::badResponse(Response::STATUS_DUPLICATE, $comment, $addon);
    }

    public static function locked($comment=null, $addon=null){
        return self::badResponse(Response::STATUS_LOCKED, $comment, $addon);
    }

    public static function error($comment=null, $addon=null){
        return self::badResponse(Response::STATUS_ERROR, $comment, $addon);
    }

    public static function badResponse($status, $comment=null, $addon=null){
        $data = [
            'status' => $status,
        ];
        if ($comment != null){
            $data['comment']  = $comment;
        }

        if (isset($addon)){
            $data['addon'] = $addon;
        }
        return $data;
    }

}