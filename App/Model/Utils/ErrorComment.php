<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 20.01.2020
 * Time: 16:22
 */

namespace App\Model\Utils;


class ErrorComment
{
    const BAD_TOKEN = [
        'RU' => 'Bad token!',
        'EN' => 'Bad token!',
    ];

    const EMAIL_NOT_FOUND = [
        'RU' => 'На Вашем аккаунте отсутствует почта, добавьте почту и повторите попытку!',
        'EN' => 'There is no mail on your account, add mail and try again!',
    ];

    const SOCIAL_EMAIL_NOT_FOUND = [
        'RU' => 'Emeil not found!',
        'EN' => 'Emeil not found!',
    ];

    const USER_NOT_REG = [
        'RU' => 'Вы не зарегистрированы!',
        'EN' => 'You not registered!',
    ];

    const SHORT_PASS = [
        'RU' => 'Короткий пароль!',
        'EN' => 'Short pass!',
    ];

    const EVENT_CLOSED = [
        'RU' => 'Ивент закрыт!',
        'EN' => 'Event closed!',
    ];

    const PHONE_NOT_ACCESS = [
        'RU' => 'Номер телефон не подтвержден!',
        'EN' => 'Phone not access!',
    ];

}