<?php
namespace App\Model\Utils;


/**
 * Class Validation
 * @package App\Model\Utils
 */
class Validation{

    /**
     * @param $phone
     * @return bool
     */
    public function phone($phone) {
        if (!is_numeric($phone)){
            return false;
        }
		return true;
	}

    /**
     * @param $id
     * @return bool
     */
    public function id($id) {
		if (!is_numeric($id) ||
		   (int)$id == 0){
			return false;
		}
		return true;
	}


    /**
     * @param $numb
     * @return bool
     */
    public function numberLogic($numb) {
		if (!is_numeric($numb) ||
            (int) $numb < 0 ||
            (int) $numb > 1){
			return false;
		}
		return true;
	}

    /**
     * @param $email
     * @return bool
     */
    public function email($email) {
		if (filter_var($email, FILTER_VALIDATE_EMAIL)){
			return true;
		}
		return false;
	}

    /**
     * @param array $data
     * @param array $keys
     * @return bool
     */
    public function arrayKeysExists(array $data, array $keys){
        foreach ($keys as $key){
            if (!isset($data[$key])){
                return false;
            }
        }
        return true;
    }


}