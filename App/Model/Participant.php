<?php

namespace App\Model;


use App\Library\DataBase\TableConfig;
use App\Model\Base\ObjectBaseDB;
use App\Model\Interfaces\Creator;

/**
 * Class UserVerification
 * @package App\Model
 */
class Participant extends ObjectBaseDB implements Creator
{

    function __construct(){
        $keys = [
            'id'                => [-1, TableConfig::Int],
            'user_id'           => [-1, TableConfig::Int],
            'event_id'          => [-1, TableConfig::Int],
            'status'            => [0, TableConfig::Int],
            'created'           => [0, TableConfig::Int],
        ];

        parent::__construct(new TableConfig($keys, EVENT_PARTICIPANT));
    }

    public static function Load($id) : self
    {
        $obj = new Participant();
        $obj->_load($id);
        return $obj;
    }
    public static function Create(array $data) : self
    {
        $obj = new Participant();
        $obj->toObject($obj->castType($obj->issetColumns($data)));
        return $obj;
    }
    public static function Search(array $data) : self
    {
        $obj = new Participant();
        $obj->loadLike($data);
        return $obj;
    }

    public function getDataToUser($keys = null)
    {
        if ($keys == null){
            $keys = ['id', 'event_id', 'status', 'created'];
        }
        return $this->getData($keys);
    }
}
