<?php

namespace App\Model;


use App\Library\DataBase\ROM\MySQL;
use App\Library\DataBase\TableConfig;
use App\Model\Base\ObjectBaseDB;
use App\Model\Interfaces\Creator;

/**
 * Class UserVerification
 * @package App\Model
 */
class Talk extends ObjectBaseDB implements Creator
{

    function __construct(){
        $keys = [
            'id'                => [-1, TableConfig::Int],
            'user_id_1'         => [-1, TableConfig::Int],
            'user_id_2'         => [-1, TableConfig::Int],
            'read_user_1'       => [0, TableConfig::Int],
            'read_user_2'       => [0, TableConfig::Int],
            'event_id'          => [-1, TableConfig::Int],
            'message'           => ['[]', TableConfig::String],
        ];

        parent::__construct(new TableConfig($keys, TALK));
    }

    public static function Load($id) : self
    {
        $obj = new Talk();
        $obj->_load($id);
        return $obj;
    }

    public static function Create(array $data) : self
    {
        $obj = new Talk();
        $obj->toObject($obj->castType($obj->issetColumns($data)));
        return $obj;
    }

    public static function Search(array $data) : self
    {
        $obj = new Talk();
        $obj->loadLike($data);
        return $obj;
    }

    public static function GetTalks(int $userID1, int $userID2, MySQL $db) : array
    {
        $talkArr = $db->orWhere('user_id_1', $userID1)->orWhere('user_id_2', $userID2)->get(TALK);
        $talks = [];
        foreach ($talkArr as $talk){
            $talks[] = Talk::Create($talk);
        }

        return $talks;
    }

    public static function GetTalkToUser(int $userID1, int $userID2, MySQL $db) : array
    {
        $talks = Talk::GetTalks($userID1, $userID2, $db);
        $res = [];
        foreach ($talks as $talk){
            if ($talk instanceof Talk){
                $res[] = $talk->convertToUser($userID1);
            }
        }
        return $res;
    }

    public function convertToUser(int $userID){
        $message = new Message([$this->data['user_id_1'], $this->data['user_id_2']]);
        $message->parse($this->data['message']);
        return [
            'id' => $this->get('id'),
            'message' => $message->getData(),
            'read' => $this->data['user_id_1'] == $userID ? $this->data['read_user_1'] : $this->data['read_user_2']
        ];
    }

    public function getTalk(int $userID) : array
    {
        $message = new Message([$this->data['user_id_1'], $this->data['user_id_2']]);
        $message->parse($this->data['message']);

        return [
            'id' => $this->data['id'],
            'event_id' => $this->data['event_id'],
            'message' => $message->getData(),
            'read' => $this->data['user_id_1'] == $userID ? $this->data['read_user_1'] : $this->data['read_user_2']
        ];
    }

    public function read(int $userID) : bool
    {
        if ($this->data['user_id_1'] == $userID){
            return $this->update(['read_user_1'=>0]);
        } else {
            return $this->update(['read_user_2'=>0]);
        }
    }

    public function setMessage(int $senderID, string $message){
        $talkMessage = new Message([$this->data['user_id_1'], $this->data['user_id_2']]);
        $talkMessage->parse($this->data['message']);
        //print_log(__FILE__, __LINE__, $userID);

        $talkMessage->addMessage($senderID, $message);

        $dataUpdate = [];
        $senderID !== $this->data['user_id_1'] ?
            $dataUpdate['read_user_1'] = $this->data['read_user_1']+1 :
            $dataUpdate['read_user_2'] = $this->data['read_user_2']+1;
        //print_log(__FILE__, __LINE__, $dataUpdate);
        $dataUpdate['message']  = $talkMessage->toString();
        //print_log(__FILE__, __LINE__, $dataUpdate['message']);
        //return true;
        return $this->update($dataUpdate);
    }
}
