<?php

namespace App\Model;


use App\Library\DataBase\TableConfig;


/**
 * Class UserVerification
 * @package App\Model
 */
class Filter
{
    protected $config;
    protected $data;
    protected $keys;
    function __construct(){
        $keys = [
            'sport_kind'        => [[], TableConfig::Arr],
            'type_event'        => [[], TableConfig::Arr],
            'type_participant'  =>[[], TableConfig::Arr],
            'country'           => [[], TableConfig::Arr],
            'city'              => [[], TableConfig::Arr],
            'date'              => [[0,0], TableConfig::Arr],
            'age'               => [[4,60], TableConfig::Arr],
        ];
        $this->config = new TableConfig($keys, "");
        $this->keys = $this->config->getColumns();
    }


    public static function Create()
    {
        $obj = new Filter();
        $obj->toObject($obj->castType($obj->issetColumns([])));
        return $obj;
    }

    public static function LoadArr(string $data)
    {
        $obj = new Filter();
        $data2 = json_decode($data, true);
        $data = [
            'sport_kind'       => $data2[0],
            'type_event'       => $data2[1],
            'type_participant' => $data2[2],
            'country'          => $data2[3],
            'city'             => $data2[4],
            'date'             => $data2[5],
            'age'              => $data2[6]
        ];
        $obj->toObject($obj->castType($obj->issetColumns($data)));
        return $obj;
    }

    public static function LoadObject(array $data){
        $obj = new Filter();
        $obj->toObject($obj->castType($obj->issetColumns($data)));
        return $obj;
    }

    public function toString(){
        return json_encode([
            $this->data['sport_kind'],
            $this->data['type_event'],
            $this->data['type_participant'],
            $this->data['country'],
            $this->data['city'],
            $this->data['date'],
            $this->data['age'],
        ]);
    }

    public function toObject(array $data)
    {
        foreach ($data as $key => $val){
            $this->data[$key] = $val;
        }
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    protected function castType(array $tableData) : array
    {
        $dataCast = [];
        foreach ($this->keys as $key) {
            if (!isset($tableData[$key])){continue;}
            $dataCast[$key] = $this->checkType($key, $tableData[$key]);
        }
        return $dataCast;
    }

    protected function issetColumns(array $tableData) : array
    {
        return isset_columns($tableData, $this->config->getColumns(), $this->config->getDefault());
    }


    /**
     * Приведение к типу
     * @param $column
     * @param $val
     * @return mixed
     */
    protected function checkType($column, $val){

        switch ($this->config->getType($column)){
            case TableConfig::String:
                return (string)$val;

            case TableConfig::Int:
                return (int)$val;

            case TableConfig::Float:
                return (double)$val;

            case TableConfig::Boolean:
                return (int)$val;

            case TableConfig::Arr:
                if (!is_array($val)){
                    return json_decode($val);
                }
                return $val;
        }

        return $val;
    }


    public function get(string $key){
        return $this->data[$key];
    }


}
