<?php

namespace App\Model;


use App\Library\DataBase\TableConfig;


/**
 * Class UserVerification
 * @package App\Model
 */
class Message
{
    protected $data;
    protected $user_id1;
    protected $user_id2;


    protected $keys = [
        'sender',
        'message',
        'created',
    ];
    function __construct(array $users){
        $this->data = [];
        $this->user_id1 = $users[0];
        $this->user_id2 = $users[1];
        //print_log(__FILE__, __LINE__, [$this->user_id1, $this->user_id2]);
    }


    public function parse(string $data)
    {
        $data2 = json_decode($data, true);
        foreach ($data2 as $d){
            $this->data[]=[
                'user_id' => $d[0] == 1? $this->user_id1 : $this->user_id2,
                'message' => $d[1],
                'created' => $d[2]
            ];
        }
    }

    public function toString()
    {
        $data = [];
        //print_log(__FILE__, __LINE__, $this->data);
        foreach ($this->data as $d){
            //print_log(__FILE__, __LINE__, [$d['id'], $this->user_id1]);
            $data[]=[
                $d['user_id'] == $this->user_id1 ? 1 : 2,
                $d['message'],
                $d['created']
            ];
        }

        return json_encode($data);
    }

    public function addMessage(int $senderID, string $message){
        //print_log(__FILE__, __LINE__, $this->user_id1);
        $add = [
            'user_id' => $senderID,
            'message' => $message,
            'created' => time()
        ];
        //print_log(__FILE__, __LINE__, $add);
        $this->data[]= $add;
        return $add;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }


    public function get(string $key){
        return $this->data[$key];
    }


}
