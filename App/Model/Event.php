<?php

namespace App\Model;


use App\Library\DataBase\TableConfig;
use App\Model\Base\ObjectBaseDB;
use App\Model\Interfaces\Creator;

/**
 * Class UserVerification
 * @package App\Model
 */
class Event extends ObjectBaseDB implements Creator
{
    const STATUS_CREATE = 0;
    const STATUS_UPDATE = 1;
    const STATUS_DELETE = 2;

    function __construct(){
        $keys = [
            'id'                => [-1, TableConfig::Int],
            'sport_kind_id'     => [1, TableConfig::Int],
            'type_event_id'     => [1, TableConfig::Int],
            'gender'            => [0, TableConfig::Int],
            'published'         => [0, TableConfig::Int],
            'date_start'        => [0, TableConfig::Int],
            'type_participant_id'=> [1, TableConfig::Int],
            'date_stop'         => [0, TableConfig::Int],
            'participant_sum'   => [0, TableConfig::Int],
            'status'            => [1, TableConfig::Int],
            'name'              => ['', TableConfig::String],
            'participant_ante'  => ['', TableConfig::String],
            'description_ante'  => ['', TableConfig::String],
            'prize_pool'        => ['', TableConfig::String],
            'country_id'        => [-1, TableConfig::Int],
            'city_id'           => [-1, TableConfig::Int],
            'description'       => ['', TableConfig::String],
            'created'           => [0, TableConfig::Int],
            'user_id'           => [-1, TableConfig::Int],
            'participant_count' => [0, TableConfig::Int],
            'age_min'           => [4, TableConfig::Int],
            'age_max'           => [60, TableConfig::Int],
            'icon'              => ['', TableConfig::String]
        ];

        parent::__construct(new TableConfig($keys, EVENT));
    }

    public static function Load($id) : self
    {
        $obj = new Event();
        $obj->_load($id);
        return $obj;
    }
    public static function Create(array $data) : self
    {
        $obj = new Event();
        $obj->toObject($obj->castType($obj->issetColumns($data)));
        return $obj;
    }
    public static function Search(array $data) : self
    {
        $obj = new Event();
        $obj->loadLike($data);
        return $obj;
    }


}
