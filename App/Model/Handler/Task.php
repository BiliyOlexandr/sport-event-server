<?php
/**
 * Created by PhpStorm.
 * UserVerification: rarog
 * Date: 11.03.2019
 * Time: 10:43
 */

namespace App\Model\Handler;

use App\Globals\GlobalData;
use App\Library\DataBase\ROM\MySQL;

/**
 * Class Task
 * @package App\Model\Handler
 */
class Task
{
    public $action;
    public $id;
    public $who;
    public $upData;

    /**
     * Task constructor.
     * @param $id
     * @param $action
     * @param $who
     */
    public function __construct($id, $action, $who=null)
    {
        $this->id = $id;
        $this->action = $action;
        $this->who = $who;
        $this->upData = [];
    }

    /**
     * @param MySQL $mySQL
     * @return bool
     */
    public function run(MySQL $mySQL)
    {
        return $this->{$this->action}($mySQL);
    }


    protected function delCar(){
        $car = Car::Load($this->id);
        if ($car->data === false){return true;}

        $tableDriver = &GlobalData::getInstance()->getTable(DRIVERS);
        $drivers = $tableDriver->where('car_id', $car->data['id'])->get();
        if (count($drivers) > 0){
            foreach ($drivers as  $driver){
                if ($driver['on_line'] == 0){
                    $tableDriver->where('id', $driver['id'])->update(['car_id'=>-1], GlobalData::getInstance()->getDb());
                }
            }
            return false;
        } else {
            $car->delete();
            return true;
        }
    }

    /**
     * @return bool
     */

    protected function delDriver(){
        $driver = Driver::Load($this->id);
        if ($driver->getId() === false){return true;}
        if ($driver->getOnLine() == 1){return false;}

        $id = $driver->firedDb(Driver::FIRED_ADMIN);
        $driver->delete();
        return true;
    }

    protected function updateDriver(){
        $driver = Driver::Load($this->id);
        if ($driver->getId() === false){return true;}
        if ($driver->getOnLine() == 1){return false;}

        $driver->update($this->upData);
        return true;
    }

    protected function updateCar(){
        $car = Car::Load($this->id);
        if ($car->data === false){return true;}


        $tableDriver = &GlobalData::getInstance()->getTable(DRIVERS);
        $drivers = count($tableDriver->where('car_id', $car->data['id'])->get(['id']));
        if ($drivers > 0){
            return false;
        } else {
            $car->update($this->upData);
            return true;
        }

    }

    /**
     * @param array $upData
     */
    public function setUpData(array $upData)
    {
        $this->upData = $upData;
    }


}