<?php

/**
 * Created by PhpStorm.
 * UserVerification: rarog
 * Date: 31.01.2019
 * Time: 1:12
 */
namespace App\Model\Handler;
use App\Model\Utils\Util;
use Workerman\Lib\Timer;

/**
 * Class MyTask
 * @package App\Model\Handler
 */
class MyTask
{
    protected $TAG = 'MyTask';
    protected static $instance;
    protected $unit;
    protected $tasks;
    protected $DAY_SEC = 3600 * 24;

    public function __construct()
    {
        $this->tasks = [];

        $this->unit = new Util();
        self::$instance = $this;
    }

    /**
     * @return MyTask
     */
    public static function getInstance(){
        if (self::$instance != null){
            return self::$instance;
        } else {
            return new MyTask();
        }
    }

    public function run()
    {
        $timer_id1 = Timer::add(10,
            function()
            {
                //handler
            }
        );
    }



    /**
     * @param Task $task
     */
    public function setTasks(Task $task)
    {
        $this->tasks[] = $task;
    }


    /**
     * @return array
     */
    public function getTasks()
    {
        return $this->tasks;
    }

}