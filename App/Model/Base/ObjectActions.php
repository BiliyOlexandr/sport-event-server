<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 08.11.2019
 * Time: 10:34
 */

namespace App\Model\Base;


interface ObjectActions
{

    /**
     * @param array $data
     * @return bool
     */
    public function update(array $data);

    /**
     * @return int
     */
    public function save();
    /**
     * @param int $id
     */
    public function _load(int $id);
    /**
     * @return bool
     */
    public function delete();
    /**
     * @param array $data
     */
    public function loadLike(array $data);

}