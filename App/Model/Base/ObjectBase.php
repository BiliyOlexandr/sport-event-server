<?php

namespace App\Model\Base;



class ObjectBase {

    protected $keys;
    protected $table;
    protected $data = null;

    function __construct($table){
        $this->table = $table;
    }

    /**
     * @return int
     */
    public function getId():int
    {
        if ($this->data == null || !isset($this->data['id'])){
            return -1;
        }

        return $this->data['id'];
    }

    /**
     * @param array $data
     */
    public function toObject(array $data)
    {
        foreach ($data as $key => $val){
            $this->data[$key] = $val;
        }
    }


    public function toArray(array $keys) : array
    {
        return array_sub($this->data, $keys);
    }


    /**
     * @return null|array
     */
    public function getData()
    {
        return $this->data;
    }


}
