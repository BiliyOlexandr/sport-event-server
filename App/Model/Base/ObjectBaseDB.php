<?php
/**
 * Created by PhpStorm.
 * UserVerification: rarog
 * Date: 16.05.2019
 * Time: 22:32
 */

namespace App\Model\Base;

use App\Globals\GlobalData;
use App\Library\DataBase\TableConfig;
use Exception;

/**
 * Class ObjectBaseDB
 * Базовый класс строк хранящихся в БД MySQL
 * @package App\Model\Base
 */
class ObjectBaseDB extends ObjectBase implements ObjectActions
{

    /**
     * @var TableConfig
     */
    protected $config;

    public function __construct(TableConfig $config)
    {
        parent::__construct($config->getTable());
        $this->config = $config;
        $this->keys = $this->config->getColumns();
    }

    /**
     * @param array $data
     * @return bool
     */
    public function update(array $data) : bool
    {
        $data = $this->castType($data);
        try {
            $res = GlobalData::getInstance()->getDb()->where('id', $this->data['id'])->update($this->table, $data);
            //print_log(__FILE__, __LINE__, GlobalData::getInstance()->getDb()->getLastQuery());
        } catch (Exception $e) {
            $res = false;
        }

        if ($res) {
            $this->toObject($data);
        }

        return $res;
    }

    /**
     * @return int
     */
    public function save() : int
    {
        $data = $this->data;
        if (isset($data['id'])){
            unset($data['id']);
        }

        //print_log(__FILE__, __LINE__, $data);
        try {
            $id = GlobalData::getInstance()->getDb()->insert($this->table, $data);
        } catch (Exception $e) {
            //print_log(__FILE__, __LINE__, $e->getMessage());
            $id = -1;
        }


        //print_log(__FILE__, __LINE__, $id);
        $this->data = $data;
        $this->data['id'] = $id;
        return $id;
    }

    /**
     * @param int $id
     */
    public function _load(int $id)
    {
        try {
            $d = GlobalData::getInstance()->getDb()->where('id', $id)->getOne($this->table);
        } catch (Exception $e) {
            $d = false;
        }
        if (isset($d) && $d !== false){
            $this->data = $d;
        }
    }


    /**
     * @return bool
     */
    public function delete() : bool
    {
        try {
            return GlobalData::getInstance()->getDb()->where('id', $this->data['id'])->delete($this->table);
        } catch (Exception $e) {
            return false;
        }
    }


    /**
     * Проверка существования стобцов в строке
     * @param array $tableData
     * @return array
     */
    protected function issetColumns(array $tableData) : array
    {
        return isset_columns($tableData, $this->config->getColumns(), $this->config->getDefault());
    }

    /**
     * Приведение к типу
     * @param array $tableData
     * @return array
     */
    protected function castType(array $tableData) : array
    {
        $dataCast = [];
        foreach ($this->keys as $key) {
            if (!isset($tableData[$key])){continue;}
            $dataCast[$key] = $this->checkType($key, $tableData[$key]);
        }
        return $dataCast;
    }

    /**
     * Приведение к типу
     * @param $column
     * @param $val
     * @return mixed
     */
    protected function checkType($column, $val){

        switch ($this->config->getType($column)){
            case TableConfig::String:
                return (string)$val;

            case TableConfig::Int:
                return (int)$val;

            case TableConfig::Float:
                return (double)$val;

            case TableConfig::Boolean:
                return (int)$val;

            case TableConfig::Arr:
                if (!is_array($val)){
                    return json_decode($val);
                }
                return $val;
        }

        return $val;
    }

    /**
     * @param array $data
     */
    public function loadLike(array $data)
    {
        try {
            $res = GlobalData::getInstance()->getDb()->where($data)->getOne($this->config->getTable());
        } catch (Exception $e) {
            $res = null;
        }
        if ($res !== null){
            $this->data = $res;
        }
    }


    /**
     * @param array $keys
     * @return null
     */
    public function getData(array $keys = [])
    {
        if (count($keys) == 0){
            return $this->data;
        } else {
            return array_sub($this->data, $keys);
        }
    }

    /**
     * @param null $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }



    public function get($key){
        return $this->data[$key];
    }

    public function set($key, $data)
    {
        $this->data[$key] = $data;
    }
}