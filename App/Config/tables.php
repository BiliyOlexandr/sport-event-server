<?php
define("CITY",              "city");
define("COUNTRY",		    "country");
define("EVENT",			    "event");
define("EVENT_COMPLAINT",	"event_complaint");
define("EVENT_PARTICIPANT",	"event_participant");
define("EVENT_RECALL",		"event_recall");
define("FOLLOWER",		    "follower");
define("ORDERS",	 	    "orders");
define("RECOVERY_PASS",	 	"recovery_pass");
define("SPORT_KIND",	    "sport_kind");
define("TALK",              "talk");
define("TYPE_EVENT",        "type_event");
define("TYPE_PARTICIPANT",  "type_participant");
define("USER",              "user");




