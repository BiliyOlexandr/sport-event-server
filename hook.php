<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 20.06.2019
 * Time: 18:12
 * @param string $key
 * @param string $target
 * @return string
 */

function saveFile(string $key, string $target){

    return  rename($_FILES[$key]['tmp_name'], $target);
}

function convertPNGToJPG($filePath){
    $image = imagecreatefrompng($filePath);
    $bg = imagecreatetruecolor(imagesx($image), imagesy($image));
    imagefill($bg, 0, 0, imagecolorallocate($bg, 255, 255, 255));
    imagealphablending($bg, TRUE);
    imagecopy($bg, $image, 0, 0, 0, 0, imagesx($image), imagesy($image));
    imagedestroy($image);

    unlink($filePath);
    $quality = 50; // 0 = worst / smaller file, 100 = better / bigger file
    imagejpeg($bg, $filePath . ".jpg", $quality);
    imagedestroy($bg);
}

function send($data=''){
    $header = apache_request_headers();
    $aHeaders = [];
    foreach ($header as $key => $value) {
        if ($key == 'Authorization') {
            $aHeaders[] = $key . ": " . $value;
        }
    }

    $url = 'http://api.sportevent.global.tmp2/'.str_replace('/hook.php', '', $_SERVER['REQUEST_URI']);

    if (isset($header['Authorization'])){
        $aHeaders[] =  "Authorization: " . $header['Authorization'];
    }

//createEvent updateEvent
    $mathes=[];
    preg_match('/api\/[a-z]{0,}\/[a-zA-Z]{0,}/', $url, $mathes);
    if(count($mathes) > 0){
        $mathes = explode('/', $mathes[0]);
    }
    if(!file_exists($_SERVER['DOCUMENT_ROOT'].'/App/Controllers/'.ucfirst(strval($mathes[1])).'/'.ucfirst(strval($mathes[1])).'Controller.php')){
        exit('{}');
    }
    require_once $_SERVER['DOCUMENT_ROOT'].'/single.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/App/Model/Filter.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/App/Library/DataBase/TableConfig.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/App/Model/Interfaces/Creator.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/App/Model/Base/ObjectActions.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/App/Model/Base/ObjectBase.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/App/Model/Base/ObjectBaseDB.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/App/Model/User.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/App/Model/Utils/Validation.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/App/Model/Utils/Util.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/App/Controllers/Base/BaseController.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/App/Controllers/Base/BaseURIRequest.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/App/Controllers/Common/CommonRequest.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/vendor/composer/vendor/thingengineer/mysqli-database-class/MysqliDb.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/App/Library/DataBase/ROM/MySQL.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/App/Globals/GlobalData.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/App/Controllers/Injection.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/App/Controllers/'.ucfirst(strval($mathes[1])).'/'.ucfirst(strval($mathes[1])).'Controller.php';

    $inection = new \App\Controllers\Injection($mathes[2], $data);
    $inection->db = \App\Globals\GlobalData::getInstance()->getDb();
    $user = new App\Controllers\User\UserController($inection);
    $user::Run($inection);
    $path = 'App\Controllers\\'.ucfirst(strval($mathes[1])).'\\'.ucfirst(strval($mathes[1])).'Controller';
    $controller = new $path($inection);
    $method = ucfirst(strval($mathes[2]));

    $response = $controller->$method();

    if(strpos($url, 'createEvent') !== false || strpos($url, 'updateEvent')!== false ){
        $res = json_decode($response, true);
        if ($res['status'] == 200 && count($_FILES) > 0) {
            $extension = explode('.', $_FILES['icon']['name'])[1];

            $filePath = __DIR__.'/src/img/events/'.$res['event']['id'].'.'.$extension;
            saveFile('icon', $filePath);
            if (strtoupper($extension) == 'PNG'){
                convertPNGToJPG($filePath);
            }
        }
    }

    echo $response;
}

$header = apache_request_headers();
$aHeaders = array();
foreach ($header as $key=>$value) {
    $aHeaders[] = $key.": ".$value;
}


if ($_SERVER['REQUEST_METHOD'] === 'POST'){
    send($_POST);
}
