<?php

define('DIR', __DIR__.'/');

require_once(DIR.'App/Config/app.php');
require_once(DIR.'App/Config/tables.php');
require_once(DIR.'App/Library/Variable/bootstrap.php');
require DIR . 'vendor/autoload.php';

use App\Controllers\Common\CommonController;
use App\Controllers\User\UserController;
use App\Controllers\Injection;
use App\Globals\GlobalData;
use App\Model\Utils\Response;
use Workerman\Connection\ConnectionInterface;
use Workerman\Protocols\Http;
use Workerman\Worker;

GlobalData::$timeZone = 0;


gc_collect_cycles();

// #### http worker ####
//Worker::$daemonize = GlobalData::$mode = true;

GlobalData::$isTestServer = false;
$worker = new Worker('http://'.URL_MAIN.":".SOCKET_MAIN);
//$worker = new Worker("http://0.0.0.0:".SOCKET_MAIN);
//$worker->transport = 'ssl';


// 1 processes
$worker->count = 1;

$worker->name = 'SportEvent';

$worker->reloadable = false;


$worker->onBufferFull = function()
{
    print_log(__FILE__, __LINE__, 'Buffer full');
};

$worker->onBufferDrain = function()
{
    print_log(__FILE__, __LINE__, 'Buffer drain');
};

$worker->onWorkerStart = function($worker)
{
    GlobalData::Create();
    //MyTask::getInstance()->run();
};

$worker->onMessage = function(ConnectionInterface $connection, $data) {
    /*set_error_handler(function ($errno, $errstr, $errfile, $errline ) {
        $exception = new ErrorException($errstr, $errno, 0, $errfile, $errline);
        LoggerBug::error('Error App', $exception);
    });
    error_reporting(E_ALL);*/

    //$time_start =  microtime(true)*1000;

    $requestUri = explode('/', trim($_SERVER['REQUEST_URI'],'/'));
    if(array_shift($requestUri) !== 'api'){
        $connection->send(json_encode(Response::badURI()));
        return;
    }
    $controller = array_shift($requestUri);
    $action = array_shift($requestUri);


    if ( isset($data['server']['HTTP_AUTHORIZATION']) ){
        $token = $data['server']['HTTP_AUTHORIZATION'];
    } else {
        $token = null;
    }

    $injection = new Injection(
        $action,
        $_REQUEST,
        $token);

    print_log(__FILE__, __LINE__, $_SERVER['REQUEST_URI'].'-'.$token.'-'.$action.'-'.$controller.'-'.json_encode($_REQUEST));
    Http::header('Access-Control-Allow-Origin: *');
    switch($controller)
    {
        case "user":
            $api = UserController::Run($injection);
            break;
        case "common":
            $api = CommonController::Run($injection);
            break;
        default:
            $api = null;
    }
    //print_log(__FILE__, __LINE__, 'SubscribesList-'.json_encode(GlobalData::getInstance()->getPhoneVerification()->getPins()));

    if ($api == null){
        $connection->send(json_encode(Response::badData('Controller not found')));
    } else {
        $response = $api->getResponse();
        //print_log(__FILE__, __LINE__, time());
        print_log(__FILE__, __LINE__, $response);
        $connection->send(json_encode($response));//JSON_NUMERIC_CHECK
    }
};


Worker::runAll();



